import React from "react";
import Dashboard from "../../pages/dashboard/Dashboard";

const DashboardRoute = () => {
  return <Dashboard />;
};

export default DashboardRoute;
