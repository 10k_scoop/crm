import React from "react";
import CreateCompanyForm from "../../pages/dashboard/CreateCompany/CreateCompanyForm";

const CreateCompanyRoute = () => {
  return <CreateCompanyForm />;
};

export default CreateCompanyRoute;
