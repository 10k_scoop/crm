import React from "react";
import Signup from "../../pages/auth/Signup";

const SignupRoute = () => {
  return <Signup />;
};

export default SignupRoute;
