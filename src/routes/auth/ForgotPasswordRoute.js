import React from "react";
import ForgotPassword from "../../pages/auth/ForgotPassword";

const ForgotPasswordRoute = () => {
  return <ForgotPassword />;
};

export default ForgotPasswordRoute;
