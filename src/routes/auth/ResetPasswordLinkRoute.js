import React from "react";
import ResetPasswordLink from "../../pages/auth/ResetPasswordLink";

const ResetPasswordLinkRoute = () => {
  return <ResetPasswordLink />;
};

export default ResetPasswordLinkRoute;
