import { Route, Router, Switch } from "react-router-dom";
import { Redirect } from "react-router";
import "./App.css";
import history from "./pages/auth/history";
import ResetPasswordLink from "./pages/auth/ResetPasswordLink";
import CreateCompanyRoute from "./routes/dashboard/CreateCompanyRoute";
import ForgotPasswordRoute from "./routes/auth/ForgotPasswordRoute";
import LoginRoute from "./routes/auth/LoginRoute";
import SignupRoute from "./routes/auth/SignupRoute";
import DashboardRoute from "./routes/dashboard/DashboardRoute";
import EditCompanyRoute from "./routes/dashboard/EditCompanyRoute";
import Cookies from 'js-cookie'
import CompanyDashboard from "./pages/Company-Dashboard/CompanyDashboard";
import CreateBranch from "./pages/Company-Dashboard/CreateBranch/CreateBranch";
import EditBranch from "./pages/Company-Dashboard/EditBranch/EditBranch";
import ShowRoles from "./pages/Company-Dashboard/Roles/ShowRoles";
import CreateRoles from "./pages/Company-Dashboard/Roles/CreateRoles";
import EditRoles from "./pages/Company-Dashboard/Roles/EditRoles";
import ShowContacts from "./pages/Company-Dashboard/Contacts/ShowContacts";
import EditContact from "./pages/Company-Dashboard/Contacts/EditContact";
import CreateContact from "./pages/Company-Dashboard/Contacts/CreateContact";
import ShowCustomers from "./pages/Company-Dashboard/Customers/ShowCustomers";
import EditCustomer from "./pages/Company-Dashboard/Customers/EditCustomer";
import CreateCustomer from "./pages/Company-Dashboard/Customers/CreateCustomer";
import CreateOwner from "./pages/Company-Dashboard/Property-Owners/CreateOwner";
import EditOwner from "./pages/Company-Dashboard/Property-Owners/EditOwner";
import ShowOwners from "./pages/Company-Dashboard/Property-Owners/ShowOwners";
import ShowProperties from "./pages/Company-Dashboard/Property/ShowProperties";
import CreateProperty from "./pages/Company-Dashboard/Property/CreateProperty";
import EditProperty from "./pages/Company-Dashboard/Property/EditProperty";
import ShowTenants from "./pages/Company-Dashboard/Tenants/ShowTenants";
import CreateTenant from "./pages/Company-Dashboard/Tenants/CreateTenant";
import EditTenant from "./pages/Company-Dashboard/Tenants/EditTenant";
import ShowEmployees from "./pages/Company-Dashboard/Employees/ShowEmployees";
import EditEmployee from "./pages/Company-Dashboard/Employees/EditEmployee";
import CreateEmployee from "./pages/Company-Dashboard/Employees/CreateEmployee";
import Settings from "./pages/Company-Dashboard/Settings/Settings";
import ShowUsers from "./pages/Company-Dashboard/Users/ShowUsers";
import CreateUser from "./pages/Company-Dashboard/Users/CreateUser";
import EditUser from "./pages/Company-Dashboard/Users/EditUser";
import CreateUserWithRole from "./pages/Company-Dashboard/Users/CreateUserWithRole";
import RevisionTask from "./pages/Company-Dashboard/Contacts/RevisionTask";
import ChatScreen from "./pages/Company-Dashboard/ChatScreen/ChatScreen";
import CallLogs from "./pages/Company-Dashboard/CallLogs/CallLogs";
import ShowTenancyContracts from "./pages/Company-Dashboard/Tenancycontracts/ShowTenancyContracts";
import ShowAttendance from "./pages/Company-Dashboard/Attendance/ShowAttendance";
import CreateAttendance from "./pages/Company-Dashboard/Attendance/CreateAttendance";
import EditAttendance from "./pages/Company-Dashboard/Attendance/EditAttendance";
import ShowProjects from "./pages/Company-Dashboard/Projects/ShowProjects";
import EditProject from "./pages/Company-Dashboard/Projects/EditProject";
import CreateProject from "./pages/Company-Dashboard/Projects/CreateProject";

function App() {

  //Check user logged in token 
  let auth_Token = Cookies.get('token')
  let role = Cookies.get('userRole')

  if (auth_Token != null) {
    if (!JSON.parse(role).includes(1)) {
      return (
        <Router history={history}>
          <Switch>
            <Route path="/dashboard">
              <CompanyDashboard />
            </Route>
            <Route path="/createBranch">
              <CreateBranch />
            </Route>
            <Route path="/editBranch">
              <EditBranch />
            </Route>
            <Route path="/showRoles">
              <ShowRoles />
            </Route>
            <Route path="/createRole">
              <CreateRoles />
            </Route>
            <Route path="/editRole">
              <EditRoles />
            </Route>
            <Route path="/showContacts">
              <ShowContacts />
            </Route>
            <Route path="/editContact">
              <EditContact />
            </Route>
            <Route path="/createContact">
              <CreateContact />
            </Route>
            <Route path="/showCustomers">
              <ShowCustomers />
            </Route>
            <Route path="/editCustomer">
              <EditCustomer />
            </Route>
            <Route path="/createCustomer">
              <CreateCustomer />
            </Route>
            <Route path="/createPropertyOwner">
              <CreateOwner />
            </Route>
            <Route path="/editPropertyOwner">
              <EditOwner />
            </Route>
            <Route path="/showPropertyOwners">
              <ShowOwners />
            </Route>
            <Route path="/showProperties">
              <ShowProperties />
            </Route>
            <Route path="/createProperty">
              <CreateProperty />
            </Route>
            <Route path="/editProperty">
              <EditProperty />
            </Route>
            <Route path="/showTenants">
              <ShowTenants />
            </Route>
            <Route path="/createTenant">
              <CreateTenant />
            </Route>
            <Route path="editTenant">
              <EditTenant />
            </Route>
            <Route path="/showEmployees">
              <ShowEmployees />
            </Route>
            <Route path="/createEmployee">
              <CreateEmployee />
            </Route>
            <Route path="/editEmployee">
              <EditEmployee />
            </Route>
            <Route path="/settings">
              <Settings />
            </Route>
            <Route path="/users">
              <ShowUsers />
            </Route>
            <Route path="/createUser">
              <CreateUser />
            </Route>
            <Route path="/createUserWithRole">
              <CreateUserWithRole />
            </Route>
            <Route path="/editUser">
              <EditUser />
            </Route>
            <Route path="/revisionTask">
              <RevisionTask />
            </Route>
            <Route path="/chat">
              <ChatScreen />
            </Route>
            <Route path="/showcallLogs">
              <CallLogs />
            </Route>
            <Route path="/showTenancyContracts">
              <ShowTenancyContracts />
            </Route>
            <Route path="/showAttendance">
              <ShowAttendance />
            </Route>
            <Route path="/createAttendance">
              <CreateAttendance />
            </Route>
            <Route path="/editAttendance">
              <EditAttendance />
            </Route>
            <Route path="/showProjects">
              <ShowProjects />
            </Route>
            <Route path="/createProject">
              <CreateProject />
            </Route>
            <Route path="/editProject">
              <EditProject />
            </Route>
            <Route path="/">
              <CompanyDashboard />
            </Route>
          </Switch>
        </Router>
      );
    } else {
      return (
        <Router history={history}>
          <Switch>
            <Route path="/dashboard">
              <DashboardRoute />
            </Route>
            <Route path="/createCompany">
              <CreateCompanyRoute />
            </Route>
            <Route path="/editCompany">
              <EditCompanyRoute />
            </Route>
            <Route path="/">
              <DashboardRoute />
            </Route>
          </Switch>
        </Router>
      );
    }
  } else {

    return (
      <Router history={history}>
        <Route path="/login">
          <LoginRoute />
        </Route>
        <Switch>
          <Route path="/register">
            <SignupRoute />
          </Route>
          <Route path="/forgotPassword">
            <ForgotPasswordRoute />
          </Route>
          <Route path="/password/reset">
            <ResetPasswordLink />
          </Route>

          <Route path='*' exact={true} component={null} >
            <Redirect exact from="*" to="/login" />
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default App;
