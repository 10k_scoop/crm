import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNavBar from "../components/SideNavBar";
import { Form } from "react-bootstrap";
import { editCompany } from "../../../state-management/actions/auth/authActions";
import { useLocation } from 'react-router-dom'
const EditCompany = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const location = useLocation()
    const { data } = location.state

    const [companyData, setCompanyData] = useState(
        {
            email: data.email,
            name: data.name,
            id:data.id
        }
    );

    const onEdit = (e) => {
        if (companyData.name != null && companyData.email != null) {
            e.preventDefault()
            setLoading(true)
            props.editCompany(companyData, setLoading)
        }
    }

    //change image to blob
    const fileToDataUri = (file) => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = (e) => {
            companyData.logo = e.target.result
        };
        reader.readAsDataURL(file);
    })

    const onPickImage = (e) => {
        let files = e.target.files || e.dataTransfer.files;
        if (!files.length)
            return;
        fileToDataUri(files[0]);
        companyData.logo = URL.createObjectURL(e.target.files[0])
    }


    const onChange = (e) => {
        const { name, value } = e.target;
        setCompanyData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };


    useEffect(() => {
        console.log(props.set_companies)
    }, [props])

    return (
        <div className="createCompanyBodyWrapper ">

            {/* Loader */}
            {loading &&
                <div className="freeLoader">
                    <Spinner animation="grow" size="lg" color="blue" />
                    <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                </div>
            }

            {/* NavBar starts here */}
            <SideNavBar />
            {/* NavBar starts here */}


            <div className="content">
                <div className="createCompanyTitle">Edit Company</div>
                <Form>
                    <Form.Group className="mb-3" controlId="name">
                        <Form.Label>Name</Form.Label>
                        <Form.Control value={companyData.name} onChange={onChange} type="text" placeholder="Name" required name="name" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="email">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control value={companyData.email} onChange={onChange} type="email" placeholder="Email" required name="email" />
                    </Form.Group>                   
                    {/* <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Upload Logo</Form.Label>
                        <Form.Control onChange={onPickImage} type="file" name="logo" />
                    </Form.Group> */}
                    <button className="btn btn-success" type="submit" onClick={onEdit}>
                        Save
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    set_companies: state.main.set_companies,
    auth: state.auth.loginRes,
});
export default connect(mapStateToProps, { editCompany })(EditCompany);

