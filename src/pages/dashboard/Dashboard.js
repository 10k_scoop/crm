import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../styles/Home.css";
import "../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Link, useHistory } from 'react-router-dom';
import { getCompanies, onLogout, deleteCompany, searchCompany } from "../../state-management/actions/auth/authActions";
import CustomTable from "./components/CustomTable";
import SideNavBar from "./components/SideNavBar";
import AlertModal from "./components/AlertModal";
import FilterBar from "./components/FilterBar";
import Header from "../Company-Dashboard/components/Header";

const Dashboard = (props) => {

    const history = useHistory();
    const [loading, setLoading] = useState(true)
    const [showModal, setShowModal] = useState(false)
    const [showSideBar, setShowSideBar] = useState(true)
    const [activeTab, setActiveTab] = useState("companies")
    const [companies, setCompanies] = useState([])
    const [selectedCompanyId, setSelectedCompanyId] = useState(null)
    

    //Companies Table Header Columns
    let companies_Cols = ['#', 'Name', 'Email', 'Logo', 'Actions']

    useEffect(() => {
        props.getCompanies(setLoading)
    }, [])
    
  
    useEffect(() => {
        if (props.get_companies != null) {
            setCompanies(props.get_companies)
            console.log(props.get_companies)
        } else {
            console.log(props)
        }
    }, [props])



    // Delete a company method
    const deleteCompany = (id) => {
        props.deleteCompany(id.id, setLoading)
        setSelectedCompanyId(null)
        setShowModal(false)
    }

    //Search Filter
    const onSearch = (val) => {
        setLoading(true)
        props.searchCompany(val, setLoading)
    }

    //onRefreshPage
    const onRefreshPage = () => {
        setLoading(true)
        props.getCompanies(setLoading)

    }

    return (
        <div className="dashboardBodyWrapper">
            {/* NavBar starts here */}
            <SideNavBar
                data={props.get_companies != null && props.get_companies[0]?.name}
                status={showSideBar}
            />
            <div className="companyDashboardContent ">


                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}


                {/* Delete confirmation alert here */}
                {showModal &&
                    <AlertModal
                        onCancel={() => {
                            setShowModal(false)
                            setSelectedCompanyId(null)
                        }}
                        onConfirm={() => deleteCompany(selectedCompanyId)}
                        name={selectedCompanyId.name}
                    />
                }
                <div className="DashboardMiddleContent container">
                    {/* Loader */}
                    {loading &&
                        <div className="freeLoader">
                            <Spinner animation="grow" size="lg" color="blue" />
                            <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                        </div>
                    }
                    <div className="createCompanyBtn">
                        <Link to="/CreateCompany" className="linkText">
                            <button className="btn btn-success notFoundText">Create company</button>
                        </Link>
                    </div>
                    <FilterBar data={companies} onSearch={(val) => onSearch(val)} onRefresh={onRefreshPage} />
                    <div className="tabs">
                        <div
                            onClick={() => setActiveTab("companies")}
                            className={activeTab == "companies" ? "tabsItemActive" : "tabsItem"}
                        >
                            My Company
                        </div>
                        <div
                            onClick={() => setActiveTab("invitation")}
                            className={activeTab == "invitation" ? "tabsItemActive" : "tabsItem"}
                        >
                            Invitations
                        </div>
                    </div>
                    {
                        companies.length >= 1 ?
                            <>
                                {
                                    activeTab == "companies" ?
                                        <CustomTable
                                            onDelete={(id) => {
                                                setShowModal(true)
                                                setSelectedCompanyId(id)
                                            }}
                                            data={companies} companies_Cols={companies_Cols} />
                                        :
                                        <p className="notFoundText">No invitations found.</p>
                                }
                            </>
                            :
                            <p className="notFoundText">You don't have any company. Create a new one.</p>
                    }
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_companies: state.main.get_companies
});
export default connect(mapStateToProps, { getCompanies, onLogout, deleteCompany, searchCompany })(Dashboard);

