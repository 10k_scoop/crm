import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { connect } from "react-redux";
import Modal from 'react-bootstrap/Modal'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRefresh, faSearch } from "@fortawesome/free-solid-svg-icons";

const FilterBar = (props) => {

    const [searchData, setSearchData] = useState({
        email: "",
        name: "",
        created_by: "",
        start_date: "",
        end_date: ""
    });
    const [filteredEmails, setFilteredEmails] = useState([])
    const [showEmailBox, setShowEmailBox] = useState(false)

    const onChange = (e) => {
        const { name, value } = e.target;
        setSearchData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
        if (name == "email") {
            setShowEmailBox(true)
        }
        setFilteredEmails(filterItems(props.data, value))
    };

    //Company Field Filter
    const filterItems = (arr, query) => {
        return arr.filter(function (el) {
            return el.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
        })
    }

    console.log(searchData)
    return (
        <div className="filterBarWrapper">
            <div className="row">
                <div className="col-sm-2">
                    <input name="name" autoComplete="off" onChange={onChange} placeholder="Name" type="text" className="filterInput" />
                </div>
                <div className="col-sm-2 emailInput">
                    <input name="email" value={searchData.email} autoComplete="off" onChange={onChange} type="email" placeholder="Email" className="filterInput" />
                    {showEmailBox &&
                        <div className="FilterDropdown">
                            {
                                filteredEmails.map((item, index) => {
                                    return (
                                        <div onClick={() => {
                                            setSearchData({
                                                email: item.email,
                                                name: null,
                                                created_by: null,
                                                start_date: null,
                                                end_date: null
                                            })
                                            setShowEmailBox(false)
                                        }
                                        }
                                            className="filterDropDownItem" key={index}>{item.email}
                                        </div>
                                    )
                                })
                            }
                        </div>
                    }
                </div>
                <div className="col-sm-2">
                    <input name="created_by" autoComplete="off" onChange={onChange} type="text" placeholder="Created By" className="filterInput" />
                </div>
                <div className="col-sm-2">
                    <input name="start_date" autoComplete="off" onChange={onChange} type="date" placeholder="Start_date" className="filterInput" />
                </div>
                <div className="col-sm-2">
                    <input name="end_date" autoComplete="off" onChange={onChange} type="date" placeholder="End Date" className="filterInput" />
                </div>
                <div className="col-sm-2">
                    <button className="btn btn-primary" onClick={() => props.onSearch(searchData)}>
                        <FontAwesomeIcon icon={faSearch} />
                        <span style={{ marginLeft: 10 }}>Search</span>
                    </button>
                    <button className="btn btn-warning" style={{ marginLeft: 10 }} onClick={props.onRefresh}>
                        <FontAwesomeIcon icon={faRefresh} />
                    </button>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
});
export default connect(mapStateToProps, {})(FilterBar);

