import { faBars, faChevronDown, faHome, faSignOut, faUserCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { onLogout } from "../../../state-management/actions/auth/authActions";

const SideNavBar = (props) => {
    const [sideNav, setSideNav] = useState(true)
    let sideNavMenuItems = [
        { name: "Dashboard", icon: faHome, redirect: "/companyDashboard" },
        { name: "Companies", icon: faUserCircle, redirect: "/companies" },
        { name: "Profile", icon: faUserCircle, redirect: "/" },
    ]

    const [activeOption, setActiveOption] = useState('Dashboard')

    return (
        <div className={props.status ? "sideNav sideNavActive" : "sideNav sideNavInActive"}>

            <p className={props.status ? "sideNavTitle sideNavItemsNoShrink" : "sideNavTitle sideNavItemsShrink"}>CRM Software</p>
            <div className={props.status ? "sideNavProfileWrapper sideNavItemsNoShrink" : "sideNavProfileWrapper sideNavItemsShrink"}>
                <div className="sideNavProfile"></div>
                <div className="sideNavProfileTitle">
                    {props?.data}
                    <FontAwesomeIcon icon={faChevronDown} style={{ marginLeft: 10 }} />
                </div>
            </div>
            <div className="sideNavMenu">
                {
                    sideNavMenuItems?.map((item, index) => {
                        return (
                            <Link to={item.redirect} key={index} onClick={() => setActiveOption(item.name)}>
                                <div className="sideNavMenuItem"
                                    style={{ borderRightColor: activeOption == item.name ? "#1eb7ff" : "white" }}
                                >
                                    <FontAwesomeIcon icon={item.icon} className="sideNavMenuIcon" />
                                    {
                                        props.status &&
                                        <span>{item.name}</span>
                                    }
                                </div>
                            </Link>
                        )
                    })
                }
            </div>

        </div>
    )
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_companies: state.main.get_companies
});
export default connect(mapStateToProps, { onLogout })(SideNavBar);


