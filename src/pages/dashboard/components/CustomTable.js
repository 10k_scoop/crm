import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faEye, faTrash } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import { Table } from "react-bootstrap";

const CustomTable = (props) => {
    return (
        <div className="companyTable">
            <Table className="table table-striped " style={{ textAlign: 'center' }} striped bordered hover>
                <thead>
                    <tr>
                        {
                            props.companies_Cols.map((item, index) => {
                                return (
                                    <th scope="col" key={index}>{item}</th>
                                )
                            })
                        }

                    </tr>
                </thead>
                <tbody>

                    {
                        props.data.map((item, index) => {
                            return (
                                <tr key={index}>
                                    <th scope="row" >{item.id}</th>
                                    <td scope="row" >{item.name}</td>
                                    <td scope="row" >{item.email}</td>
                                    <td scope="row" >
                                        {/* <img src={item.logo} style={{width:'15%',height:'15%'}} /> */}
                                        Logo
                                    </td>
                                    <td>
                                        <div className="tableActionButtons">
                                            <button className="btn btn-primary">
                                                <FontAwesomeIcon icon={faEye} />
                                            </button>
                                            <Link to={{ pathname: "/editCompany", state: { data: item } }}>
                                                <button className="btn btn-success">
                                                    <FontAwesomeIcon icon={faEdit} />
                                                </button>
                                            </Link>
                                            <button className="btn btn-danger" onClick={()=>props.onDelete({id:item.id,name:item.name})}>
                                                <FontAwesomeIcon icon={faTrash} />
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            )
                        })
                    }


                </tbody>
            </Table>
        </div>
    );
};

export default CustomTable;

