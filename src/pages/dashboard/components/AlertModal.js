import React from "react";
import { Button } from "react-bootstrap";
import { connect } from "react-redux";
import Modal from 'react-bootstrap/Modal'

const AlertModal = (props) => {
    return (
        <div className="modalBg">
            <div className="modalBgLayer"></div>
            <div className="modalWrapper">
                <div className="modalHeader">
                    Warning !
                </div>
                <div className="modalBody">
                    <h3>Are you Sure?</h3>
                    <span>Do you really want to delete this {props.name} company? This process cannot be undone.</span>
                </div>
                <div className="modalFooter">
                    <button className="btn btn-secondary" onClick={props.onCancel}>Cancel</button>
                    <button className="btn btn-warning" onClick={props.onConfirm}>Confirm</button>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
});
export default connect(mapStateToProps, {})(AlertModal);

