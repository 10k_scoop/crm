import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNavBar from "../components/SideNavBar";
import { Form } from "react-bootstrap";
import { createCompany } from "../../../state-management/actions/auth/authActions";
import Header from "../../Company-Dashboard/components/Header";

const CreateCompanyForm = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const [companyData, setCompanyData] = useState(
        {
            email: null,
            name: null,
            address: null,
            branch_name: null,
            phone: null,
            zipcode: null,
            logo: null,
            password: null
        }
    );

    const onCreate = (e) => {
        if (companyData.name != null && companyData.email != null && companyData.branch_name != null) {
            e.preventDefault()
            setLoading(true)
            props.createCompany(companyData, setLoading)
        }
    }

    //change image to blob
    const fileToDataUri = (file) => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = (e) => {
            companyData.logo = e.target.result
        };
        reader.readAsDataURL(file);
    })

    const onPickImage = (e) => {
        let files = e.target.files || e.dataTransfer.files;
        // if (!files.length)
        //     return;
        // fileToDataUri(files[0]);
        //companyData.logo = URL.createObjectURL(e.target.files[0])
        companyData.logo = files[0]
    }


    const onChange = (e) => {
        const { name, value } = e.target;
        setCompanyData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };


    useEffect(() => {
        console.log(props.set_companies)
    }, [props])

    return (
        <div className="dashboardBodyWrapper ">

            {/* Loader */}
            {loading &&
                <div className="freeLoader">
                    <Spinner animation="grow" size="lg" color="blue" />
                    <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                </div>
            }

            {/* NavBar starts here */}
            <SideNavBar
                data={props.get_companies != null && props.get_companies[0]?.name}
                status={showSideBar}
            />
            <div className="companyDashboardContent ">
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                {/* NavBar starts here */}
                <div className="DashboardMiddleContent container createCompanyForm">
                    <div className="content">
                        <div className="createCompanyTitle">Create Company</div>
                        <Form>
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control onChange={onChange} type="text" placeholder="Name" required name="name" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control onChange={onChange} type="email" placeholder="Email" required name="email" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control onChange={onChange} type="password" placeholder="password" required name="password" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="address">
                                <Form.Label>Address</Form.Label>
                                <Form.Control onChange={onChange} type="text" placeholder="Address" name="address" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="branchName">
                                <Form.Label>Branch Name</Form.Label>
                                <Form.Control onChange={onChange} type="text" placeholder="Branch Name" required name="branch_name" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="phone">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control onChange={onChange} type="number" placeholder="Phone" required name="phone" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="zipCode">
                                <Form.Label>Zip Code</Form.Label>
                                <Form.Control onChange={onChange} type="number" placeholder="Zip Code" name="zipcode" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Upload Logo</Form.Label>
                                <Form.Control onChange={onPickImage} type="file" name="logo" />
                            </Form.Group>
                            <button className="btn btn-primary" type="submit" onClick={onCreate}>
                                Create
                            </button>
                        </Form>
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    set_companies: state.main.set_companies,
});
export default connect(mapStateToProps, { createCompany })(CreateCompanyForm);

