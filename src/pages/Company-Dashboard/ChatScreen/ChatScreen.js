import React, { useEffect, useRef, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/CompanyDashboard.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import { role } from "../../../constants/CookiesData";
import { Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import ChatBox from "./components/ChatBox";
import ChatTypeBox from "./components/ChatTypeBox";
import RecieveMessage from "./components/RecieveMessage";
import SendMessage from "./components/SendMessage";

const ChatScreen = (props) => {

    const [loading, setLoading] = useState(false)
    const [showSideBar, setShowSideBar] = useState(false)
    const [selectedChat, setSelectedChat] = useState(false)
    const [inputText, setInputText] = useState("")



    const [messagesData, setMessagesData] = useState([
        { text: "Hello", type: "reciever" },
        { text: "Hello", type: "sender" },
        { text: "Hey", type: "reciever" },
        { text: "Hello", type: "reciever" },
        { text: "Whats is the status ?", type: "sender" },
        { text: "Hello", type: "reciever" },
        { text: "Hello", type: "reciever" },
        { text: "Hello", type: "sender" },
        { text: "Hey", type: "reciever" },
        { text: "Hello", type: "reciever" },
        { text: "Whats is the status ?", type: "sender" },
        { text: "Hello", type: "reciever" },
        { text: "Hello", type: "reciever" },
        { text: "Hello", type: "sender" },
        { text: "Hey", type: "reciever" },
        { text: "Hello", type: "reciever" },
        { text: "Whats is the status ?", type: "sender" },
        { text: "Whatttttt", type: "reciever" },
    ])

    const chatsData = [
        { id: 1, name: "Alexander", message: "Hello", time: "9:00 PM", role: "Admin" },
        { id: 2, name: "John Doe", message: "Hey !", time: "10:00 PM", role: "Super Admin" },
        { id: 3, name: "Lothfy", message: "Hello", time: "2:00 PM", role: "Manager" },
        { id: 4, name: "Darius", message: "Hello", time: "5:00 PM", role: "Sales Agent" },
    ]

    const messagesEndRef = useRef(null);
    const scrollToBottom = () => {
        messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
    };
    useEffect(scrollToBottom, [messagesData]);


    useEffect(() => {
        setSelectedChat(chatsData[0])
    }, [])

    const onChatPress = (id) => {
        const data = chatsData?.find(element => element.id == id);
        setSelectedChat(data)
    }

    const sendText = (text) => {
        if(text==null || text==""){
            return;
        }
        let data = { text: text, type: "sendr" }
        // messagesData.push(data)
        setMessagesData((prev) => [...prev, data])
    }


    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}

                <div className="row chatBoxWrapperMain container-fluid">
                    <div className="col-sm-3">
                        <div className="contactsSideBar">
                            <div className="contactsSideBarHeader">
                                <div className="contactsSideBarSearch">
                                    <FontAwesomeIcon icon={faSearch} color="#4f5368" />
                                    <Form.Control type="text" placeholder="Search or start new chat" />
                                </div>
                            </div>

                            {
                                chatsData.map((item, index) => {
                                    return (
                                        <ChatBox
                                            key={index}
                                            data={item}
                                            onPress={(id) => onChatPress(id)}
                                        />
                                    )
                                })
                            }


                        </div>
                    </div>

                    <div className="col-sm-9">
                        <div className="chatBoxHeader">
                            <div className="contactBoxProfile"></div>
                            <div>
                                <div className="contactBoxUserName">{selectedChat.name}</div>
                                <div className="contactBoxLastText">{selectedChat?.role}</div>
                            </div>
                        </div>
                        <div className="chatBoxBody">
                            <ChatTypeBox onChange={(e) => setInputText(e.target.value)} onSendPress={() => sendText(inputText)} />
                            <div className="messagesWrapper">
                                <div>
                                    {
                                        messagesData.map((item, index) => {
                                            if (item.type == "reciever") {
                                                return (
                                                    <RecieveMessage key={index} text={item.text} />
                                                )
                                            } else {
                                                return (
                                                    <SendMessage key={index} text={item.text} />
                                                )
                                            }

                                        })
                                    }
                                </div>
                                <div ref={messagesEndRef} />
                            </div>
                        </div>
                    </div>

                </div>

                {/* dashboard Body */}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_contacts: state.main.get_contacts
});
export default connect(mapStateToProps, {})(ChatScreen);

