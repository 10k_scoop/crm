import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

const ChatBox = (props) => {

    const [loading, setLoading] = useState(false)
    const [showSideBar, setShowSideBar] = useState(false)

    return (
        <div className="contactUserBox" onClick={()=>props.onPress(props?.data?.id)}>
            <div className="contactBoxProfile"></div>
            <div className="contactBoxUserdetailWrapper">
                <div className="contactBoxUserNameWrapper">
                    <div className="contactBoxUserName">{props?.data?.name}</div>
                    <div className="contactBoxTimeText">{props?.data?.time}</div>
                </div>
                <div className="contactBoxLastText">{props?.data?.message}</div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_contacts: state.main.get_contacts
});
export default connect(mapStateToProps, {})(ChatBox);

