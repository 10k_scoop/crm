import { faPaperclip, faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

const ChatTypeBox = (props) => {

    const [loading, setLoading] = useState(false)
    const [showSideBar, setShowSideBar] = useState(false)

    return (
        <div className="chatTypeBoxWrapper">
            <FontAwesomeIcon icon={faPaperclip} className="icon" />
            <div className="textField">
                <input onChange={props.onChange} type="text" placeholder="Type a message " />
            </div>
            <div className="Sendbtn" onClick={props.onSendPress}>
                <FontAwesomeIcon icon={faPaperPlane} />
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_contacts: state.main.get_contacts
});
export default connect(mapStateToProps, {})(ChatTypeBox);

