import { faPaperclip, faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

const RecieveMessage = (props) => {

    const [loading, setLoading] = useState(false)
    const [showSideBar, setShowSideBar] = useState(false)

    return (
        <div className="recievedMessage" onClick={() => props.onPress()}>
            {props?.text}
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_contacts: state.main.get_contacts
});
export default connect(mapStateToProps, {})(RecieveMessage);

