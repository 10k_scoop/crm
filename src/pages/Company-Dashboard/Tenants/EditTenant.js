import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { editTenant } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import { useLocation } from "react-router";

const EditTenant = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const location = useLocation()
    const { data } = location.state
    const [tenantsData, setTenantsData] = useState(
        {
            id: data.id,
            name: data?.name,
            email_address: data?.email_address,
            contact: data?.contact,
            passport_number: data?.passport_number,
            passport: data?.passport,
            emirates_id: data?.emirates_id,
            nationality: data?.nationality,
            emirates_id_file: data?.emirates_id_file,
            created_at: new Date(),

        }
    );

    const onCreate = (e) => {
        setLoading(true)
        e.preventDefault()
        props.editTenant(tenantsData, setLoading)
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setTenantsData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };



    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Edit Property</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control value={tenantsData?.name} onChange={(e) => onChange(e)} type="text" required name="name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Email</Form.Label>
                                <Form.Control value={tenantsData?.email_address} onChange={(e) => onChange(e)} type="email" required name="email_address" />
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Contact</Form.Label>
                                <Form.Control value={tenantsData?.contact} onChange={(e) => onChange(e)} type="text" required name="contact" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Passport no</Form.Label>
                                <Form.Control value={tenantsData?.passport_number} onChange={(e) => onChange(e)} type="text" required name="passport_number" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Emirates Id</Form.Label>
                                <Form.Control value={tenantsData?.emirates_id} onChange={(e) => onChange(e)} type="text" required name="emirates_id" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Nationality</Form.Label>
                                <Form.Control value={tenantsData?.nationality} onChange={(e) => onChange(e)} type="text" required name="nationality" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Passport</Form.Label>
                                <Form.Control value={tenantsData?.passport} onChange={(e) => onChange(e)} type="file" name="passport" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Emirate Id File</Form.Label>
                                <Form.Control value={tenantsData?.emirates_id_file} onChange={(e) => onChange(e)} type="file" name="emirates_id_file" />
                            </Form.Group>
                        </div>
                    </div>

                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Save
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    edit_tenant: state.main.edit_tenant
});
export default connect(mapStateToProps, { editTenant })(EditTenant);

