import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/CompanyDashboard.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNav from "../components/SideNav";
import { showTenants, deleteTenant } from "../../../state-management/actions/Features";
import Header from "../components/Header";
import { TenantColumn } from "../components/TableColumn";
import { Link } from "react-router-dom";
import GlobalTable from "../components/GlobalTable";

const ShowTenants = (props) => {

    const [loading, setLoading] = useState(true)
    const [showSideBar, setShowSideBar] = useState(true)
    const [tenantsData, showTenantsData] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);

    useEffect(() => {
        props.showTenants(setLoading)
    }, [])

    useEffect(() => {
        if (props.get_tenants != null) {
            showTenantsData(props.get_tenants)
            setfilteredData(props.get_tenants)
        } else {
            //  console.log(props)
        }
    }, [props])


    //Search
    const onSearch = (val, type) => {
        let filteredData = tenantsData.filter(function (item) {
            let name = item.name.toLowerCase().includes(val)
            let emirates_id = item.emirates_id.includes(val)
            if (name != "") {
                return name
            }
            else {
                return emirates_id
            }
        });
        setfilteredData(filteredData);
    };

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav
                status={showSideBar}
            />
            {/* SideMenu */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}

                {/* dashboard Body */}
                <div className="DashboardMiddleContent container">
                    <h2 style={{ marginBottom: 10 }}>Tenants</h2>
                    {/* Table */}
                    <Link to="/createTenant" >
                        <button className="btn btn-primary">Create Tenant</button>
                    </Link>
                    <GlobalTable
                        title="Tenants"
                        onSearch={(val) => {
                            onSearch(val)
                            setSearchText(val)
                        }}
                        onDelete={(id) => props.deleteTenant(id, setLoading)}
                        onRefresh={() => {
                            setLoading(true)
                            props.showTenants(setLoading)
                        }}
                        editPathname="/editTenant"
                        actions={["Delete", "View", "Edit"]}
                        col={TenantColumn}
                        data={searchText == "" ? tenantsData : filteredData}
                    />
                    {/* Table */}
                </div>
                {/* dashboard Body */}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_tenants: state.main.get_tenants
});
export default connect(mapStateToProps, { deleteTenant, showTenants })(ShowTenants);

