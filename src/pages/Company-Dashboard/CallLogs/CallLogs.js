import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Spinner } from "react-bootstrap";
import { showCallLogs,deleteCallLog} from "../../../state-management/actions/Features";
import GlobalTable from "../components/GlobalTable";
import { CallLogsColumn } from "../components/TableColumn";
import SideNav from "../components/SideNav";
import Header from "../components/Header";

const CallLogs = (props) => {

    const [loading, setLoading] = useState(false)
    const [showSideBar, setShowSideBar] = useState(true)
    const [callsData, setCallsData] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);

    useEffect(() => {
        if (props.get_call_logs != null) {
            setCallsData(props.get_call_logs)
            setfilteredData(props.get_call_logs)
        } else {
            //  console.log(props)
        }
    }, [props])

    useEffect(() => {
        props.showCallLogs(setLoading)
    }, [])


    //Search
    const onSearch = (val, type) => {
        let filteredData = callsData.filter(function (item) {
            let name = item.call_title.toLowerCase().includes(val)
            if (name != "") {
                return name
            }
            else {
                return null
            }
        });
        setfilteredData(filteredData);
    };

    

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav
                status={showSideBar}
                onNavClose={()=>setShowSideBar(false)}
            />
            {/* SideMenu */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="DashboardMiddleContent container">
                    <h2 style={{ marginBottom: 10 }}>Call logs</h2>
                    <div className="usersTable col-sm-12">
                        <GlobalTable
                            title="Call Logs"
                            showDeleteBtn
                            showEditBtn
                            onSearch={(val) => {
                                console.log(val)
                                onSearch(val)
                                setSearchText(val)
                            }}
                            onAssignClick={(e, type, taskId) => console.log(e, '', taskId)}
                            onDelete={(id) => props.deleteCallLog(id, setLoading)}
                            onRefresh={() => {
                                setLoading(true)
                                props.showCallLogs(setLoading)
                            }}
                            col={CallLogsColumn}
                            fullData={callsData}
                            data={searchText == "" ? callsData.data : filteredData}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_contacts: state.main.get_contacts,
    get_call_logs: state.main.get_call_logs
});
export default connect(mapStateToProps, {showCallLogs ,deleteCallLog})(CallLogs);

