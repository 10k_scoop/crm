import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/CompanyDashboard.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNav from "../components/SideNav";
import { showRoles, deleteRole } from "../../../state-management/actions/Features";
import Header from "../components/Header";
import { PermissionsColumn } from "../components/TableColumn";
import GlobalTable from "../components/GlobalTable";
import { Link } from "react-router-dom";
import { role } from "../../../constants/CookiesData";

const ShowRoles = (props) => {

    const [loading, setLoading] = useState(true)
    const [showSideBar, setShowSideBar] = useState(true)
    const [roles, setRoles] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);

    useEffect(() => {
        props.showRoles(setLoading)
    }, [])

    useEffect(() => {
        if (props.get_roles != null) {
            setRoles(props.get_roles.roles)
            setfilteredData(props.get_roles.roles)
        } else {
            console.log(props)
        }
    }, [props])

    //Search
    const onSearch = (val, type) => {
        let target = val.toLowerCase();
        let filteredData = roles.filter(function (item) {
            return item.name.toLowerCase().includes(val)

        });

        setfilteredData(filteredData);
    };
    console.log(props.get_roles)

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}

                {/* dashboard Body */}
                <div className="DashboardMiddleContent container">
                    <h2 style={{ marginBottom: 10 }}>Roles</h2>
                    {/* Table */}
                    {role() == "SuperAdmin" &&
                        <Link to={{ pathname: "/createRole", state: { Dbpermissions: props.get_roles?.permissions } }} >
                            <button className="btn btn-primary">Create Role</button>
                        </Link>
                    }
                    <GlobalTable
                        title="Roles"
                        onSearch={(val) => {
                            onSearch(val)
                            setSearchText(val)
                        }}
                        onDelete={(id) => props.deleteRole(id, setLoading)}
                        onRefresh={() => {
                            setLoading(true)
                            props.showRoles(setLoading)
                        }}

                        editPathname="/editRole"
                        permissions={props.get_roles?.permissions}
                        actions={["Delete", "View", "Edit"]}
                        col={PermissionsColumn}
                        data={searchText == "" ? roles : filteredData}
                    />
                    {/* Table */}
                </div>
                {/* dashboard Body */}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_roles: state.main.get_roles
});
export default connect(mapStateToProps, { deleteRole, showRoles })(ShowRoles);

