import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { editRole } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import Select from 'react-select'
import { useLocation } from "react-router";

const EditRole = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const location = useLocation()
    const { data, permissionsData,Hpermissions } = location?.state
    const [id, setId] = useState()
    const [permissions, setPermissions] = useState([])
    const [fullPermissionData, setFullPermissionData] = useState()
    const [name, setName] = useState(data?.name)
  
    useEffect(() => {
        try {
            data?.permissions.map((item, index) => {
                setPermissions((prevState) => ([...prevState, item.id]))
            })
            setId(data.id)
            setFullPermissionData(permissionsData)
        } catch (e) {
            console.log(e)
        }
    }, [])

    const onCreate = (e) => {
        try {
            setLoading(true)
            if (name != null && permissions != null) {
                let sendData = {
                    name: name,
                    permissions: permissions,
                    id: data?.id
                }
                e.preventDefault()
                setLoading(true)
                props.editRole(sendData, setLoading)
            }
        } catch (e) {
            console.log(e)
        }
        //console.log(data)
    }

    const onChange = (e, type) => {
        const { name, value } = e.target;
        setName(value);
    };
    useEffect(() => {
        // console.log("refresh")
    }, [permissions])
    console.log(Hpermissions)

    const onSelectPermission = async (val, checked, index) => {
        let arr = permissions;
        if (checked == "yes") {
            let index = arr.indexOf(parseInt(val))
            let slice = arr.splice(index, 1)
            await setPermissions([])
            setPermissions(arr)
            //console.log(arr)
        } else {
            setPermissions((prevState) => ([...prevState, parseInt(val)]))
        }
    }
  

    console.log(permissions)

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Edit Role</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Role Name</Form.Label>
                                <Form.Control value={name} onChange={(e) => onChange(e, "n")} type="text" placeholder="Name" required name="branch_name" />
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12">
                            <Form.Group className="mb-3 checkBox">
                                {
                                    Hpermissions?.map((item, index) => {
                                        return (
                                            <Form.Check type="checkbox" defaultChecked={permissions.includes(item.id) ? true : false} name={item.id} onChange={(val) => onSelectPermission(val.target.name, permissions.includes(item.id) ? "yes" : "no", index)} label={item.name.toUpperCase()} key={index} className="roleCheckBox" />
                                        )
                                    })
                                }
                            </Form.Group>
                        </div>
                    </div>
                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Save
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    edit_roles: state.main.edit_roles
});
export default connect(mapStateToProps, { editRole })(EditRole);

