import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { createRole } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import Select from 'react-select'
import { useLocation } from "react-router";

const CreateRole = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const [name, setName] = useState()
    const [permissions, setPermissions] = useState([])
    const location = useLocation()
    const { Dbpermissions } = location.state


    const onCreate = (e) => {
        setLoading(true)
        if (name != null && permissions != null) {
            e.preventDefault()
            setLoading(true)
            props.createRole(name, permissions, setLoading)
        }
    }

    const onChange = (e, type) => {
        const { name, value } = e.target;
        setName(value);
    };

    const onSelectPermission = (val) => {
        setPermissions((prevState) => ([...prevState,val]))
    }


    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar}/>
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Create Role</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Role Name</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="text" placeholder="Name" required name="branch_name" />
                            </Form.Group>
                        </div>

                    </div>
                    <div className="row">
                        <div className="col-sm-12">
                            <Form.Group className="mb-3 checkBox">
                                {
                                    Dbpermissions?.map((item, index) => {
                                        return (
                                            <Form.Check type="checkbox" name={item.id} onChange={(val) => onSelectPermission(val.target.name)} label={item.name.toUpperCase()} key={index} className="roleCheckBox" />
                                        )
                                    })
                                }
                            </Form.Group>
                        </div>
                    </div>

                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Save
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_roles: state.main.get_roles
});
export default connect(mapStateToProps, { createRole })(CreateRole);

