import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/CompanyDashboard.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNav from "../components/SideNav";
import { showProjects, deleteProject } from "../../../state-management/actions/Features";
import Header from "../components/Header";
import GlobalTable from "../components/GlobalTable";
import { CustomersColumn, ProjectsColumn } from "../components/TableColumn";
import { Link } from "react-router-dom";

const ShowProjects = (props) => {

    const [loading, setLoading] = useState(true)
    const [showSideBar, setShowSideBar] = useState(true)
    const [projects, setProjects] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);
    const [showModal, setShowModal] = useState(false);

    useEffect(() => {

        props.showProjects(setLoading)

    }, [])


    useEffect(() => {
        if (props.get_projects != null) {
            setProjects(props.get_projects)
            setfilteredData(props.get_projects)
        } else {
            console.log(props)
        }
    }, [props])

    //Search
    const onSearch = (val, type) => {
        let filteredData = projects.filter(function (item) {
            let name = item.name.toLowerCase().includes(val)
            let phone = item.phone.includes(val)
            let landing_page_link = item.landing_page_link.toLowerCase().includes(val)
            let email = item.email.toLowerCase().includes(val)
            let location = item.location.toLowerCase().includes(val)
            if (name != "") {
                return name

            } else if (email != "") {

                return email

            } else if (landing_page_link != "") {

                return landing_page_link
            } else if (phone != "") {

                return phone
            }
            else {
                return location
            }
        });
        setfilteredData(filteredData);
    };

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} onNavClose={()=>setShowSideBar(false)} />
            {/* SideMenu */}

            {/* Popup Modal */}
            {
                showModal &&
                <div className={showModal ? "assignModalWrapper" : "assignModalWrapperActive"}>
                    <div className="assignModalLayer"></div>
                    <div className="assignModal">
                        <div className="assignModalHeader"></div>
                        <div className="assignModalBody"></div>
                        <div className="assignModalHeader row">
                            <div className="col-sm-12">
                                <button className="btn btn-danger" onClick={() => setShowModal(!showModal)}>Cancel</button>
                                <button className="btn btn-warning">Assign</button>
                            </div>
                        </div>
                    </div>
                </div>
            }
            {/* Popup Modal */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}

                {/* dashboard Body */}
                <div className="DashboardMiddleContent container">
                    <h2 style={{ marginBottom: 10 }}>Projects</h2>
                    <Link to="/createProject">
                        <button className="btn btn-primary">Create Project</button>
                    </Link>
                    {/* Table */}
                    <GlobalTable
                        showEditBtn
                        showDeleteBtn
                        createBtn={true}
                        onAssignClick={() => setShowModal(!showModal)}
                        title="Projects"
                        onSearch={(val) => {
                            console.log(val)
                            onSearch(val)
                            setSearchText(val)
                        }}
                        onDelete={(id) => props.deleteProject(id, setLoading)}
                        onRefresh={() => {
                            setLoading(true)
                            props.showProjects(setLoading)
                        }}
                        editPathname="/editProject"
                        actions={["Assign", "Delete", "View", "Edit"]}
                        col={ProjectsColumn}
                        data={searchText == "" ? projects : filteredData}
                    />
                    {/* Table */}
                </div>
                {/* dashboard Body */}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_projects: state.main.get_projects
});
export default connect(mapStateToProps, { deleteProject, showProjects })(ShowProjects);

