import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { editProject } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import { useLocation } from "react-router";

const EditProject = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const location = useLocation()
    const { data } = location.state
    const [projectData, setProjectData] = useState(
        {
            project_name: data?.project_name,
            description: data?.description,
            status: data?.status,
            type: data?.type,
            created_at: new Date(),
        }
    );


    const onCreate = (e) => {
        setLoading(true)
        e.preventDefault()
        props.editProject(projectData, setLoading)
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setProjectData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };



    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Edit Project</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="project_name">
                                <Form.Label>Project Name</Form.Label>
                                <Form.Control value={projectData?.project_name} onChange={(e) => onChange(e)} type="text" required name="project_name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="description">
                                <Form.Label>Description</Form.Label>
                                <Form.Control value={projectData?.description} onChange={(e) => onChange(e)} type="text" required name="description" />
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="status">
                                <Form.Label>Status</Form.Label>
                                <Form.Control value={projectData?.status} onChange={(e) => onChange(e)} type="text" required name="status" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="type">
                                <Form.Label>Type</Form.Label>
                                <Form.Control value={projectData?.type} onChange={(e) => onChange(e)} type="text" required name="type" />
                            </Form.Group>
                        </div>
                    </div>

                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Save
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    edit_project: state.main.edit_project
});
export default connect(mapStateToProps, { editProject })(EditProject);

