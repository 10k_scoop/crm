import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/profile.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import Profile from "./Profile"
import Billing from "./Billing";
import Security from "./Security";
import Notification from "./Notification";
const Settings = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const [activeOption, setActiveOption] = useState("profile")
    const [profileData, setProfileData] = useState(
        {
            name: "",
            email_address: "",
            contact: "",
            passport_number: "",
            passport: "",
            emirates_id: "",
            nationality: "",
            emirates_id_file: "",
            created_at: new Date(),

        }
    );

    const onCreate = (e) => {
        setLoading(true)
        e.preventDefault()
        // props.createProfile(profileData, setLoading)
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setProfileData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };


    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} data={props.get_branches != null && props.get_branches[0]?.name} />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">{activeOption.toUpperCase()}</div>
                <div className="container-xl px-4 mt-4">
                    {/* Account page navigation*/}
                    <nav className="nav nav-borders">
                        <div className={"nav-link settingMenuItems active ms-0"}
                            onClick={() => setActiveOption("profile")}
                        >
                            Profile
                        </div>
                        <div className="nav-link settingMenuItems" href="#billing" target="__blank"
                            onClick={() => setActiveOption("billing")}
                        >
                            Billing
                        </div>
                        <div className="nav-link settingMenuItems" onClick={() => setActiveOption("security")}>Security</div>
                        <div className="nav-link settingMenuItems" onClick={() => setActiveOption("notification")}>Notifications</div>
                    </nav>
                    <hr className="mt-0 mb-4" />
                    {/* Profile Screen */}
                    {activeOption == "profile" ?
                        < Profile />
                        :
                        activeOption == "billing" ?
                            <Billing />
                            : activeOption == "security" ?
                                <Security />
                                : <Notification />

                    }
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
});
export default connect(mapStateToProps, {})(Settings);

