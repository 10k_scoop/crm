import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { createBranch } from "../../../state-management/actions/auth/authActions";
import SideNav from "../components/SideNav";
import { faHome, faTree, faUser, faUsers } from "@fortawesome/free-solid-svg-icons";
import Header from "../components/Header";
import Cookies from 'js-cookie'

const CreateBranch = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    let companyDetails = Cookies.get('companyDetails')
    const [branchData, setBranchData] = useState(
        {
            email: null,
            address: null,
            branch_name: null,
            phone: null,
            zipcode: null,
            company_id: props.get_branches[0].user_id
        }
    );

    const onCreate = (e) => {
        if (branchData.company_id != null && branchData.email != null && branchData.branch_name != null) {
            e.preventDefault()
            setLoading(true)
            props.createBranch(branchData, setLoading)
        }
    }
    
    const onChange = (e) => {
        const { name, value } = e.target;
        setBranchData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}


            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Create Branch</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Branch Name</Form.Label>
                                <Form.Control onChange={onChange} type="text" placeholder="Name" required name="branch_name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control onChange={onChange} type="email" placeholder="Email" required name="email" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="address">
                                <Form.Label>Address</Form.Label>
                                <Form.Control onChange={onChange} type="text" placeholder="Address" name="address" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="zipCode">
                                <Form.Label>Zip Code</Form.Label>
                                <Form.Control onChange={onChange} type="number" placeholder="Zip Code" name="zipcode" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="phone">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control onChange={onChange} type="number" placeholder="Phone" required name="phone" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">

                        </div>
                    </div>
                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Create
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches:state.main.get_branches
});
export default connect(mapStateToProps, { createBranch })(CreateBranch);

