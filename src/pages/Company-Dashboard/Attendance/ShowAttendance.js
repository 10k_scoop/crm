import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Spinner } from "react-bootstrap";
import {showAttendance} from "../../../state-management/actions/Features";
import GlobalTable from "../components/GlobalTable";
import { AttendanceColumn, CallLogsColumn } from "../components/TableColumn";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import { Link } from "react-router-dom";

const ShowAttendance = (props) => {

    const [loading, setLoading] = useState(false)
    const [showSideBar, setShowSideBar] = useState(true)
    const [attendances, setAttendances] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);

    useEffect(() => {
        if (props.get_attendance != null) {
            setAttendances(props.get_attendance)
            setfilteredData(props.get_attendance)
        } else {
            //  console.log(props)
        }
    }, [props])

    useEffect(() => {
        props.showAttendance(setLoading)
    }, [])


    //Search
    const onSearch = (val, type) => {
        let filteredData = attendances.filter(function (item) {
            let name = item.date.toLowerCase().includes(val)
            if (name != "") {
                return name
            }
            else {
                return null
            }
        });
        setfilteredData(filteredData);
    };

    

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav
                status={showSideBar}
                onNavClose={()=>setShowSideBar(false)}
            />
            {/* SideMenu */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="DashboardMiddleContent container">
                    <h2 style={{ marginBottom: 10 }}>Attendance</h2>
                    <div className="usersTable col-sm-12">
                    <div className="col-sm-2">
                            <Link to="/createAttendance">
                                <button className="btn btn-primary">Create Attendance</button>
                            </Link>
                        </div>
                        <GlobalTable
                            title="Attendance"
                            onSearch={(val) => {
                                console.log(val)
                                onSearch(val)
                                setSearchText(val)
                            }}
                            editPathname="/editAttendance"
                            onAssignClick={(e, type, taskId) => console.log(e, '', taskId)}
                            onDelete={(id) => props.deleteAttendance(id, setLoading)}
                            onRefresh={() => {
                                setLoading(true)
                                props.showAttendance(setLoading)
                            }}
                            col={AttendanceColumn}
                            fullData={attendances}
                            data={searchText == "" ? attendances.data : filteredData}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_contacts: state.main.get_contacts,
    get_attendance: state.main.get_attendance
});
export default connect(mapStateToProps, {showAttendance})(ShowAttendance);

