import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { createAttendance } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import { role, role as userRole } from "../../../constants/CookiesData";
import { AdminFeatures,SuperAdminFeatures} from "../../../constants/FeaturesList";

const CreateAttendance = (props) => {

    const [loading, setLoading] = useState(false)
    const [showSideBar, setShowSideBar] = useState(true)
    const [attendanceData, setAttendanceData] = useState(
        {
            date: "",
            total_hours: "",
            employee_id: "",
            branch_id: "",
        }
    );

 

    const onCreate = (e) => {
        setLoading(true)
        e.preventDefault()
        props.createAttendance(attendanceData, setLoading)
        console.log(attendanceData)
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setAttendanceData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };



    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav
                data={props.get_branches != null && props.get_branches[0]?.name}
                status={showSideBar}
                menuItems={userRole() == "Admin" ? AdminFeatures : SuperAdminFeatures}
                onNavClose={()=>setShowSideBar(false)}
            />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Create Attendance</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Employee Id</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="text" required name="employee_id" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Branch Id</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="email" required name="branch_id" />
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Date</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="date" required name="date" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Total Hours</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="numbers" required name="total_hours" />
                            </Form.Group>
                        </div>
                    </div>

                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Create
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    create_attendance: state.main.create_attendance,
});
export default connect(mapStateToProps, { createAttendance })(CreateAttendance);

