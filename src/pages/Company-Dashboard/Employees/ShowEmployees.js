import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/CompanyDashboard.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNav from "../components/SideNav";
import { showEmployees, deleteEmployee } from "../../../state-management/actions/Features";
import Header from "../components/Header";
import { EmployeeColumn } from "../components/TableColumn";
import GlobalTable from "../components/GlobalTable";
import { Link } from "react-router-dom";


const ShowEmployees = (props) => {

    const [loading, setLoading] = useState(true)
    const [showSideBar, setShowSideBar] = useState(true)
    const [employees, setEmployees] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);

    useEffect(() => {
        props.showEmployees(setLoading)
    }, [])

    useEffect(() => {
        if (props.get_employees != null) {
            setEmployees(props.get_employees)
            setfilteredData(props.get_employees)
        } else {
            console.log(props)
        }
    }, [props])


    //Search
    const onSearch = (val, type) => {
        let filteredData = employees.filter(function (item) {
            let name = item.name.toLowerCase().includes(val)
            let email_address = item.email_address.includes(val)
            if (name != "") {
                return name
            }
            else {
                return email_address
            }
        });
        setfilteredData(filteredData);
    };

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}

                {/* dashboard Body */}
                <div className="DashboardMiddleContent container">
                    <h2 style={{ marginBottom: 10 }}>Employees</h2>
                    {/* Table */}

                    <Link to="/createEmployee">
                        <button className="btn btn-primary"> Create Employee</button>
                    </Link>
                    <GlobalTable
                        title="Employees"
                        onSearch={(val) => {
                            onSearch(val)
                            setSearchText(val)
                        }}
                        onDelete={(id) => props.deleteEmployee(id, setLoading)}
                        onRefresh={() => {
                            setLoading(true)
                            props.showEmployees(setLoading)
                        }}
                        editPathname="/editEmployee"
                        actions={["Delete", "View", "Edit"]}
                        col={EmployeeColumn}
                        data={searchText == "" ? employees : filteredData}
                    />
                    {/* Table */}
                </div>
                {/* dashboard Body */}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_employees: state.main.get_employees
});
export default connect(mapStateToProps, { deleteEmployee, showEmployees })(ShowEmployees);

