import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { createCustomer } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";

const CreateCustomer = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const [customerData, setCustomerData] = useState(
        {
            first_name: "",
            email: "",
            phone_number: "",
            last_name: "",
            street_address: "",
            location: "",
            landing_page_link: "",
            created_at: new Date(),

        }
    );

    const onCreate = (e) => {
        setLoading(true)
        e.preventDefault()
        props.createCustomer(customerData, setLoading)
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setCustomerData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };



    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Create Customer</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="first_name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Email</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="email" required name="email" />
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="phone_number" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Location</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="street_address" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Website | Source</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="landing_page_link" />
                            </Form.Group>
                        </div>
                    </div>

                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Create
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    create_customer: state.main.create_customer
});
export default connect(mapStateToProps, { createCustomer })(CreateCustomer);

