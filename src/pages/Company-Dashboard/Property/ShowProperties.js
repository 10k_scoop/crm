import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/CompanyDashboard.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNav from "../components/SideNav";
import { showProperties, deleteProperty } from "../../../state-management/actions/Features";
import Header from "../components/Header";
import { Link } from "react-router-dom";
import GlobalTable from "../components/GlobalTable";
import { propertyColumn } from "../components/TableColumn";


const ShowProperties = (props) => {

    const [loading, setLoading] = useState(true)
    const [showSideBar, setShowSideBar] = useState(true)
    const [properties, setProperties] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);

    useEffect(() => {
        props.showProperties(setLoading)
    }, [])

    useEffect(() => {
        if (props.get_properties != null) {
            setProperties(props.get_properties)
            setfilteredData(props.get_properties)
        } else {
            console.log(props)
        }
    }, [props])


    //Search
    const onSearch = (val, type) => {
        let filteredData = properties.filter(function (item) {
            let name = item.name.toLowerCase().includes(val)
            let tower_name = item.tower_name.includes(val)
            if (name != "") {
                return name
            }
            else {
                return tower_name
            }
        });
        setfilteredData(filteredData);
    };

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}

                {/* dashboard Body */}
                <div className="DashboardMiddleContent container">
                    <h2 style={{ marginBottom: 10 }}>Properties</h2>
                    {/* Table */}
                    <Link to="/createProperty" >
                        <button className="btn btn-primary">Create Property</button>
                    </Link>
                    <GlobalTable
                        title="Properties"
                        onSearch={(val) => {
                            onSearch(val)
                            setSearchText(val)
                        }}
                        onDelete={(id) => props.deleteProperty(id, setLoading)}
                        onRefresh={() => {
                            setLoading(true)
                            props.showProperties(setLoading)
                        }}
                        editPathname="/editProperty"
                        actions={["Delete", "View", "Edit"]}
                        col={propertyColumn}
                        data={searchText == "" ? properties : filteredData}
                    />
                    {/* Table */}
                </div>
                {/* dashboard Body */}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_properties: state.main.get_properties
});
export default connect(mapStateToProps, { deleteProperty, showProperties })(ShowProperties);

