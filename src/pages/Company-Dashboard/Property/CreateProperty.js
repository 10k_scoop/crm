import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { createProperty } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";

const CreateProperty = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const [propertyData, setPropertyData] = useState(
        {
            name: "",
            tower_name: "",
            unit_number: "",
            floor_number: "",
            parking_slot: "",
            description: "",
            number_of_bedrooms: "",
            kitchen: "",
            hall: "",
            furnished: "",
            status: "",
            created_at: new Date(),

        }
    );

    const onCreate = (e) => {
        setLoading(true)
        e.preventDefault()
        props.createProperty(propertyData, setLoading)
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setPropertyData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };



    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Create Property</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Tower Name</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="email" required name="tower_name" />
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Unit Number</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="unit_number" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Parking Slot</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="parking_slot" />
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Floor Number</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="floor_number" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Number of bedroom</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="number_of_bedrooms" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Hall</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="hall" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Kitchen</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="kitchen" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Status</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="status" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Furnished</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="furnished" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Description</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="description" />
                            </Form.Group>

                        </div>
                        <div className="col-sm-6">

                        </div>
                    </div>

                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Create
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    create_property: state.main.create_property
});
export default connect(mapStateToProps, { createProperty })(CreateProperty);

