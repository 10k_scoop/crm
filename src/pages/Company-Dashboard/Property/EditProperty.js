import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { editProperty } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import { useLocation } from "react-router";

const EditProperty = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const location = useLocation()
    const { data } = location.state
    const [propertyData, setPropertyData] = useState(
        {
            id:data.id,
            name: data.name,
            tower_name: data.tower_name,
            unit_number: data.unit_number,
            floor_number: data.floor_number,
            parking_slot: data.parking_slot,
            description: data.description,
            number_of_bedrooms: data.number_of_bedrooms,
            kitchen: data.kitchen,
            hall: data.hall,
            furnished: data.furnished,
            status: data.status,
            created_at: new Date(),

        }
    );

    const onCreate = (e) => {
        setLoading(true)
        e.preventDefault()
        props.editProperty(propertyData, setLoading)
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setPropertyData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };



    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Edit Property</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control value={propertyData.name} onChange={(e) => onChange(e)} type="text" required name="name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Tower Name</Form.Label>
                                <Form.Control value={propertyData.tower_name} onChange={(e) => onChange(e)} type="email" required name="tower_name" />
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Unit Number</Form.Label>
                                <Form.Control value={propertyData.unit_number} onChange={(e) => onChange(e)} type="text" required name="unit_number" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Parking Slot</Form.Label>
                                <Form.Control value={propertyData.parking_slot} onChange={(e) => onChange(e)} type="text" required name="parking_slot" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Description</Form.Label>
                                <Form.Control value={propertyData.description} onChange={(e) => onChange(e)} type="text" required name="description" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Number of bedroom</Form.Label>
                                <Form.Control value={propertyData.number_of_bedrooms} onChange={(e) => onChange(e)} type="text" required name="number_of_bedrooms" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Kitchen</Form.Label>
                                <Form.Control value={propertyData.kitchen} onChange={(e) => onChange(e)} type="text" required name="kitchen" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Hall</Form.Label>
                                <Form.Control value={propertyData.hall} onChange={(e) => onChange(e)} type="text" required name="hall" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Furnished</Form.Label>
                                <Form.Control value={propertyData.furnished} onChange={(e) => onChange(e)} type="text" required name="furnished" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Status</Form.Label>
                                <Form.Control value={propertyData.status} onChange={(e) => onChange(e)} type="text" required name="status" />
                            </Form.Group>
                        </div>
                    </div>

                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Save
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    edit_property: state.main.edit_property
});
export default connect(mapStateToProps, { editProperty })(EditProperty);

