import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/CompanyDashboard.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNav from "../components/SideNav";
import { showTenancyContracts, deleteTenancyContract } from "../../../state-management/actions/Features";
import Header from "../components/Header";
import GlobalTable from "../components/GlobalTable";
import { CustomersColumn, TenancyContractsColumn } from "../components/TableColumn";

const ShowTenancyContracts = (props) => {

    const [loading, setLoading] = useState(true)
    const [showSideBar, setShowSideBar] = useState(true)
    const [contracts, setContracts] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);
    const [showModal, setShowModal] = useState(false);

    useEffect(() => {
        props.showTenancyContracts(setLoading)
    }, [])


    useEffect(() => {
        if (props.get_tenancy_contracts != null) {
            setContracts(props.get_tenancy_contracts)
            setfilteredData(props.get_tenancy_contracts)
        } else {
            console.log(props)
        }
    }, [props])


    //Search
    const onSearch = (val, type) => {
        let filteredData = contracts.filter(function (item) {
            let name = item.name.toLowerCase().includes(val)

            if (name != "") {
                return name
            }
            else {
                return null
            }
        });
        setfilteredData(filteredData);
    };

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}

            {/* Popup Modal */}
            {
                showModal &&
                <div className={showModal ? "assignModalWrapper" : "assignModalWrapperActive"}>
                    <div className="assignModalLayer"></div>
                    <div className="assignModal">
                        <div className="assignModalHeader"></div>
                        <div className="assignModalBody"></div>
                        <div className="assignModalHeader row">
                            <div className="col-sm-12">
                                <button className="btn btn-danger" onClick={() => setShowModal(!showModal)}>Cancel</button>
                                <button className="btn btn-warning">Assign</button>
                            </div>
                        </div>
                    </div>
                </div>
            }
            {/* Popup Modal */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}

                {/* dashboard Body */}
                <div className="DashboardMiddleContent container">
                    <h2 style={{ marginBottom: 10 }}>Tenancy Contracts</h2>
                    {/* Table */}
                    <GlobalTable
                        onAssignClick={() => setShowModal(!showModal)}
                        title="Customers"
                        onSearch={(val) => {
                            console.log(val)
                            onSearch(val)
                            setSearchText(val)
                        }}
                        onDelete={(id) => props.deleteTenancyContract(id, setLoading)}
                        onRefresh={() => {
                            setLoading(true)
                            props.showTenancyContracts(setLoading)
                        }}
                        editPathname="/editCustomer"
                        actions={["Assign", "Delete", "View", "Edit"]}
                        col={TenancyContractsColumn}
                        data={searchText == "" ? contracts.data : filteredData}
                    />
                    {/* Table */}
                </div>
                {/* dashboard Body */}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_tenancy_contracts: state.main.get_tenancy_contracts
});
export default connect(mapStateToProps, { showTenancyContracts, deleteTenancyContract })(ShowTenancyContracts);

