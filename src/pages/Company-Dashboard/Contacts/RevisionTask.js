import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/CompanyDashboard.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNav from "../components/SideNav";
import { showRevisionTask, showUsers } from "../../../state-management/actions/Features";
import Header from "../components/Header";
import { Link } from "react-router-dom";
import { useLocation } from "react-router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserCircle } from "@fortawesome/free-solid-svg-icons";

const RevisionTask = (props) => {

    const [loading, setLoading] = useState(true)
    const [showSideBar, setShowSideBar] = useState(true)
    const [taskData, setTaskData] = useState([])
    const [users, setUsers] = useState([])
    const location = useLocation()
    const { data } = location.state

    useEffect(() => {
        props.showRevisionTask(data.id, setLoading)
        props.showUsers(setLoading)
    }, [])

    useEffect(() => {
        if (props.get_revision_task != null) {
            setTaskData(props.get_revision_task)
            setUsers(props.get_users)
        } else {
            console.log(props)
        }
    }, [props])


    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}

                {/* dashboard Body */}
                <div className="DashboardMiddleContent container">
                    <h2 style={{ marginBottom: 10 }}>Task Revision</h2>
                    {/* Contact Card Wrapper */}
                    <div className="contactCardWrapper row">
                        {/* Contact Card */}
                        <div className="col-sm-6">
                            <div className="contactCard">
                                <div className="row">
                                    <div className="col-sm-3">
                                        <div className="contactCardProfile">
                                            <FontAwesomeIcon icon={faUserCircle} color="white" />
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="first_name">{taskData.data?.first_name?.toUpperCase()}</div>
                                        <div className="last_name">{taskData.data?.last_name?.toUpperCase()}</div>
                                    </div>
                                </div>

                                <div className="row deatilRows" style={{ marginTop: 20 }}>
                                    <div className="col-sm-5">
                                        <span className="cardContactLabel">Email</span>
                                    </div>
                                    <div className="col-sm-6">
                                        <span className="cardContactLabelValue">{taskData.data?.email?.toUpperCase()}</span>
                                    </div>
                                </div>
                                <div className="row deatilRows" style={{ marginTop: 20 }}>
                                    <div className="col-sm-5">
                                        <span className="cardContactLabel">Job Title</span>
                                    </div>
                                    <div className="col-sm-6">
                                        <span className="cardContactLabelValue">{taskData.data?.job_title?.toUpperCase()}</span>
                                    </div>
                                </div>
                                <div className="row deatilRows" style={{ marginTop: 20 }}>
                                    <div className="col-sm-5">
                                        <span className="cardContactLabel">Company Name</span>
                                    </div>
                                    <div className="col-sm-6">
                                        <span className="cardContactLabelValue">{taskData.data?.company_name?.toUpperCase()}</span>
                                    </div>
                                </div>
                                <div className="row deatilRows" style={{ marginTop: 20 }}>
                                    <div className="col-sm-5">
                                        <span className="cardContactLabel">Contact</span>
                                    </div>
                                    <div className="col-sm-6">
                                        <span className="cardContactLabelValue">{taskData.data?.phone_number?.toUpperCase()}</span>
                                    </div>
                                </div>

                                <div className="row deatilRows" style={{ marginTop: 20 }}>
                                    <div className="col-sm-5">
                                        <span className="cardContactLabel">Website</span>
                                    </div>
                                    <div className="col-sm-6">
                                        <span className="cardContactLabelValue">{taskData.data?.landing_page_link?.toUpperCase()}</span>
                                    </div>
                                </div>

                                <div className="row deatilRows" style={{ marginTop: 20 }}>
                                    <div className="col-sm-5">
                                        <span className="cardContactLabel">Address</span>
                                    </div>
                                    <div className="col-sm-6">
                                        <span className="cardContactLabelValue">{taskData.data?.street_address?.toUpperCase()}</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                        {/* Contact Card */}

                        {/* Contact Card History Tree */}
                        <div className="col-sm-6">
                            <div className="hitoryTreeWrapper">
                                {
                                    taskData.data?.revision_task.map((item, index) => {

                                        const assigned_to_id = props.get_users?.find(element => element.id == item.assigned_to_id);
                                        const assigned_by_id = props.get_users?.find(element => element.id == item.assigned_by_id);
                                        return (
                                            <div key={index}>
                                                <div className="statusDetail">
                                                    Assigned By: {assigned_by_id?.name}
                                                </div>
                                                <div className="statusDetail">
                                                    Assigned To : {assigned_to_id?.name}
                                                </div>
                                                <div className="hitoryTreeCircelItem">
                                                    {item?.leadstatus?.name}
                                                </div>
                                                {
                                                    taskData?.data?.revision_task?.length > index + 1 &&
                                                    <div className="hitoryTreeCircelItemVr"></div>
                                                }
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                        {/* Contact Card History Tree */}
                    </div>
                    {/* Contact Card  Wrapper*/}
                </div>
                {/* dashboard Body */}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_revision_task: state.main.get_revision_task,
    get_users: state.main.get_users
});
export default connect(mapStateToProps, { showRevisionTask, showUsers })(RevisionTask);

