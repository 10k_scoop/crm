import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { createContact } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import { role, role as userRole } from "../../../constants/CookiesData";
import { AdminFeatures, SuperAdminFeatures } from "../../../constants/FeaturesList";
import { getMyBranches } from "../../../state-management/actions/auth/authActions";

const CreateContact = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const [name, setName] = useState()
    const [branches, setBranches] = useState([])
    const [contactData, setContactData] = useState(
        {
            company_name: "",
            email: "",
            first_name: "",
            last_name: "",
            job_title: "",
            street_address: "",
            phone_number: "",
            mobile_phone_number: "",
            lead_status_id: 1,
            branch_id: null,
            contact_owner_id: 1,

        }
    );

    useEffect(() => {
        props.getMyBranches(setLoading)
    }, [])

    useEffect(() => {
        if (props.get_branches != null) {
            setBranches(props.get_branches)
        } else {
            console.log(props)
        }
    }, [props])

    const onCreate = (e) => {
        setLoading(true)
        e.preventDefault()
        props.createContact(contactData, setLoading)
        console.log(contactData)
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setContactData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };



    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav
                data={props.get_branches != null && props.get_branches[0]?.name}
                status={showSideBar}
                menuItems={userRole() == "Admin" ? AdminFeatures : SuperAdminFeatures}
                onNavClose={()=>setShowSideBar(false)}
            />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Create Contact</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Company Name</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="text" required name="company_name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Email</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="email" required name="email" />
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="text" required name="first_name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="text" required name="last_name" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Job Title</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="text" required name="job_title" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="number" required name="phone_number" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Mobile Phone</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="number" required name="mobile_phone_number" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Street Address</Form.Label>
                                <Form.Control onChange={(e) => onChange(e, "n")} type="text" required name="street_address" />
                            </Form.Group>
                        </div>
                        {
                            role() == "SuperAdmin" &&
                            <div className="col-sm-6">
                                <Form.Group className="mb-3" controlId="branche_id">
                                    <Form.Label>Branch</Form.Label>
                                    <Form.Select onChange={(e) => onChange(e, "n")} name="branche_id" >
                                        <option value={null}>select branch</option>
                                        {
                                            branches?.map((item, index) => {

                                                return (
                                                    <option key={index} value={item.id}>{item.name}</option>
                                                )
                                            })
                                        }
                                    </Form.Select>
                                </Form.Group>
                            </div>
                        }
                    </div>

                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Create
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    create_contact: state.main.create_contact,
    get_branches: state.main.get_branches
});
export default connect(mapStateToProps, { createContact, getMyBranches })(CreateContact);

