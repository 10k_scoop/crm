import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { editContact } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import { role as userRole } from "../../../constants/CookiesData";
import { AdminFeatures, SuperAdminFeatures } from "../../../constants/FeaturesList";
import { useLocation } from "react-router";
const EditContact = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const [name, setName] = useState()
    const [permissions, setPermissions] = useState([])
    const location = useLocation()
    const { data } = location.state
    const [contactData, setContactData] = useState(
        {
            company_name: data.company_name,
            email: data.email,
            first_name: data.first_name,
            last_name: data.last_name,
            job_title: data.job_title,
            street_address: data.street_address,
            phone_number: data.phone_number,
            mobile_phone_number: data.mobile_phone_number,
            lead_status_id: data.lead_status_id,
            contact_owner_id: data.contact_owner_id,
            id:data.id,
            lifecycle_stage_id:1
        }
    );

    const onCreate = (e) => {
        setLoading(true)
        e.preventDefault()
        props.editContact(contactData, setLoading)
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setContactData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };


    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav
                data={props.get_branches != null && props.get_branches[0]?.name}
                status={showSideBar}
                menuItems={userRole()=="Admin"?AdminFeatures:SuperAdminFeatures} 
            />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Edit Contact</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Company Name</Form.Label>
                                <Form.Control value={contactData.company_name} onChange={(e) => onChange(e, "n")} type="text" required name="company_name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Email</Form.Label>
                                <Form.Control value={contactData.email} onChange={(e) => onChange(e, "n")} type="email" required name="email" />
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control value={contactData.first_name} onChange={(e) => onChange(e, "n")} type="text" required name="first_name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control value={contactData.last_name} onChange={(e) => onChange(e, "n")} type="text" required name="last_name" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Job Title</Form.Label>
                                <Form.Control value={contactData.job_title} onChange={(e) => onChange(e, "n")} type="text" required name="job_title" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control value={contactData.phone_number} onChange={(e) => onChange(e, "n")} type="number" required name="phone_number" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Mobile Phone</Form.Label>
                                <Form.Control value={contactData.mobile_phone_number} onChange={(e) => onChange(e, "n")} type="number" required name="mobile_phone_number" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Street Address</Form.Label>
                                <Form.Control value={contactData.street_address} onChange={(e) => onChange(e, "n")} type="text" required name="street_address" />
                            </Form.Group>
                        </div>
                    </div>

                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Save
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    edit_contact: state.main.edit_contact
});
export default connect(mapStateToProps, { editContact })(EditContact);

