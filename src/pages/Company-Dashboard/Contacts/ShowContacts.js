import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/CompanyDashboard.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNav from "../components/SideNav";
import { showContacts, createTask, deleteContact, autoAssignTask, showUnassignedContacts } from "../../../state-management/actions/Features";
import Header from "../components/Header";
import { ContactsColumn, ContactsColumnSalesAgent } from "../components/TableColumn";
import GlobalTable from "../components/GlobalTable";
import { Link } from "react-router-dom";
import { Form } from "react-bootstrap";
import PopUpModal from "../components/PopUpModal";
import { role, token } from "../../../constants/CookiesData";

const ShowContacts = (props) => {

    const [loading, setLoading] = useState(true)
    const [showSideBar, setShowSideBar] = useState(true)
    const [contacts, setContacts] = useState([])
    const [salesAgents, setSalesAgents] = useState([])
    const [selectedSaleAgent, setSelectedSaleAgent] = useState()
    const [selectedContacts, setSelectedContacts] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [autoAssign, setAutoAssign] = useState(false);
    const [modalType, setModalType] = useState();
    const [selectedTab, setSelectedTab] = useState('all');

    useEffect(() => {
        setShowModal(false)
        props.showContacts(setLoading)
    }, [])

    useEffect(() => {
        if (selectedTab == "all") {
            props.showContacts(setLoading)
        } else {
            props.showUnassignedContacts(setLoading)
        }
    }, [selectedTab])

    useEffect(() => {
        if (props.get_contacts != null) {
            setContacts(props.get_contacts?.data)
            setfilteredData(props.get_contacts?.data)
            setSalesAgents(props.get_contacts?.salesAgent)
            setAutoAssign(props.get_contacts?.automaticStatus?.status)
        } else {
            console.log(props)
        }
    }, [props])

    const onSearch = (val, type) => {
        let target = val.toLowerCase();
        let filteredData = contacts.filter(function (item) {
            let cname = item.company_name.toLowerCase().includes(val)
            let phone = item.mobile_phone_number.includes(val)
            let first_name = item.first_name.toLowerCase().includes(val)
            let last_name = item.last_name.toLowerCase().includes(val)
            let street_address = item.street_address.toLowerCase().includes(val)
            let email = item.email.toLowerCase().includes(val)
            let job_title = item.job_title.toLowerCase().includes(val)

            if (cname != "") {
                return cname
            } else if (email != "") {
                return email
            } else if (street_address != "") {
                return street_address
            } else if (last_name != "") {
                return last_name
            }
            else if (job_title != "") {
                return job_title
            }
            else if (phone != "") {
                return phone
            }
            else {
                return first_name
            }

        });
        setfilteredData(filteredData);
    };

    const onSubmit = (comments) => {
        setLoading(true)
        let data = {
            assigned_to_id: selectedSaleAgent,
            contact_id: selectedContacts,
            comments:comments
        }
        props.createTask(data, setLoading, setShowModal)
    }
    const onAutoStatus = (status) => {
        // setLoading(true)
        if (status) {
            let data = {
                status: 'active'
            }
            props.autoAssignTask(data, setLoading, setAutoAssign)
        } else {
            let data = {
                status: 'inactive'
            }
            props.autoAssignTask(data, setLoading, setAutoAssign)
        }
    }

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar}  onNavClose={()=>setShowSideBar(false)} />
            {/* SideMenu */}

            {/* Popup Modal */}
            {showModal
                &&
                <div className={showModal ? "assignModalWrapper" : "assignModalWrapperActive"}>
                    <div className="assignModalLayer"></div>
                    <PopUpModal
                        showModal={showModal}
                        setShowModal={setShowModal}
                        setSelectedSaleAgent={setSelectedSaleAgent}
                        listData={salesAgents}
                        projects={props.get_contacts?.projects}
                        onSubmit={(val)=>onSubmit(val)}

                    />
                </div>
            }
            {/* Popup Modal */}




            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}

                {/* dashboard Body */}
                <div className="DashboardMiddleContent container-fluid">
                    <h2 style={{ marginBottom: 10 }}>Contacts</h2>
                    {/* Table */}

                    <div className="row">
                        <div className="col-sm-2">
                            <Link to="/createContact">
                                <button className="btn btn-primary">Create Contact</button>
                            </Link>
                        </div>
                        {/* Contact Tabs For ADMIN */}
                        {role() == "Admin" &&
                            <div className="col-sm-7" style={{ display: 'flex', alignItems: 'center' }}>
                                <div className="TabsWrapper">
                                    <div className={selectedTab == "all" ? "TabsItemActive" : "TabsItemNotActive"}
                                        onClick={() => setSelectedTab('all')}
                                    >
                                        All contacts
                                    </div>
                                    <div className={selectedTab == "UC" ? "TabsItemActive" : "TabsItemNotActive"}
                                        onClick={() => setSelectedTab('UC')}
                                    >
                                        Unassigned Contacts
                                    </div>
                                </div>
                            </div>
                        }
                        {/* Contact Tabs For ADMIN */}
                        <div className="col-sm-3" style={{ display: 'flex', alignItems: 'center' }}>
                            {role() == "Admin" &&
                                <div className="autoSwitch btn btn-warning">
                                    <span>Auto Assign</span>
                                    <label className="switch">
                                        <input
                                            checked={autoAssign == "active" ? true : false}
                                            type="checkbox"
                                            onChange={(e) => onAutoStatus(e.target.checked)}
                                        />
                                        <span className="slider round"></span>
                                    </label>
                                </div>
                            }
                        </div>
                    </div>
                    <GlobalTable
                        title="Contacts"
                        onSearch={(val) => {
                            console.log(val)
                            onSearch(val)
                            setSearchText(val)
                        }}
                        onAssignClick={(data, type) => {
                            setShowModal(!showModal)
                            setSelectedContacts(data)
                            setModalType(type)
                        }}
                        assign={true}
                        onDelete={(id) => props.deleteContact(id, setLoading)}
                        onRefresh={() => {
                            setLoading(true)
                            props.showContacts(setLoading)
                        }}
                        viewPathname={"/revisionTask"}
                        editPathname="/editContact"
                        col={role() == "SalesAgent" ? ContactsColumnSalesAgent : ContactsColumn}
                        data={searchText == "" ? contacts : filteredData}
                    />
                    {/* Table */}
                </div>
                {/* dashboard Body */}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_contacts: state.main.get_contacts
});
export default connect(mapStateToProps, { deleteContact, showUnassignedContacts, autoAssignTask, createTask, showContacts })(ShowContacts);

