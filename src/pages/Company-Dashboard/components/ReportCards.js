import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ReportCards = (props) => {

    return (
        <div className="col-sm-4">
            <div className="adminDashboardReportWrapper">
                <div className="adminDashboardReportWrapperLeft">
                    <div className="title">TOTAL {props.title}</div>
                    <div className="count">{props.num}</div>
                    <div className="bottomLeftPercentageWrapper">
                        <div className="bottomLeftPercentage">+90%</div>
                        <span>from 2,320</span>
                    </div>
                </div>
                <div className="adminDashboardReportWrapperRight">
                    <FontAwesomeIcon icon={props.icon} className="chartIcon" />
                </div>

            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_roles: state.main.get_roles
});
export default connect(mapStateToProps, {})(ReportCards);

