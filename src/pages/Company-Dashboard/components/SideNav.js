import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/CompanyDashboard.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faTimes, } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import { AdminFeatures, SalesAgentFeatures, SuperAdminFeatures } from "../../../constants/FeaturesList";
import { role as userRole, userDetails } from "../../../constants/CookiesData";
const SideNav = (props) => {

    const [activeOption, setActiveOption] = useState('Dashboard')
    const [closeNav,setCloseNav]=useState(false)
    let listItems =
        userRole() == "Admin" ?
            AdminFeatures
            : userRole() == "SalesAgent" ?
                SalesAgentFeatures
                : userRole() == "SuperAdmin" ?
                    SuperAdminFeatures
                    : null

    return (
        <div className={props.status ? "sideNav sideNavActive" : "sideNav sideNavInActive"}>

            <div style={{ display: 'flex', alignItems: 'center' }}>
                <p className={props.status ? "sideNavTitle sideNavItemsNoShrink" : "sideNavTitle sideNavItemsShrink"}>CRM Software</p>
                <div className="closeNavBtn" onClick={props.onNavClose}>
                    <FontAwesomeIcon icon={faTimes} />
                </div>
            </div>
            <div className={props.status ? "sideNavProfileWrapper sideNavItemsNoShrink" : "sideNavProfileWrapper sideNavItemsShrink"}>
                <div className="sideNavProfile"></div>
                <div className="sideNavProfileTitle">
                    {userDetails()?.name}
                    <FontAwesomeIcon icon={faChevronDown} style={{ marginLeft: 10 }} />
                    <br />
                    {userDetails()?.email}
                </div>
            </div>
            <div className="sideNavMenu">
                {
                    listItems?.map((item, index) => {
                        return (
                            <Link to={item.redirect} key={index} onClick={() => setActiveOption(item.name)}>
                                <div className="sideNavMenuItem"
                                    style={{ borderRightColor: activeOption == item.name ? "#1eb7ff" : "white" }}
                                >
                                    <FontAwesomeIcon icon={item.icon} className="sideNavMenuIcon" />
                                    {
                                        props.status &&
                                        <span>
                                            {item.name}
                                        </span>
                                    }
                                </div>
                            </Link>
                        )
                    })
                }
            </div>

        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    auth: state.auth.get_recover_password_link,
    get_branches: state.main.get_branches,
});
export default connect(mapStateToProps, {})(SideNav);

