import { faBars, faBell, faMessage, faPowerOff } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { onLogout } from "../../../state-management/actions/auth/authActions";

const Header = (props) => {

    return (
        < div className="headerContentwrapper" >
            {/* <div className="headerLayer"></div> */}
            <div className="row">
                <div className="col-sm-9">
                    <FontAwesomeIcon icon={faBars} className="sideNavBtn" onClick={() => props.setShowSideBar(!props.showSideBar)} />
                </div>
                <div className="col-sm-2 headerRightIconsWrapper">
                    <div className="headerBellIconWrapper">
                        <div className="headerBellIconCount">4</div>
                        <FontAwesomeIcon icon={faBell} className="sideNavBtn" />
                    </div>
                    <div className="headerBellIconWrapper">
                        <Link to="/chat">
                            <div className="headerBellIconCount">4</div>
                            <FontAwesomeIcon icon={faMessage} className="sideNavBtn" />
                        </Link>
                    </div>
                    <div className="headerBellIconWrapper" onClick={props.onLogout}>
                        <FontAwesomeIcon icon={faPowerOff} className="sideNavBtn" />
                    </div>
                </div>

            </div>
        </div >
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    auth: state.auth.get_recover_password_link,
});
export default connect(mapStateToProps, { onLogout })(Header);

