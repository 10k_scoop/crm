import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Form } from "react-bootstrap";

const PopUpModal = (props) => {
    const [comment, setComment] = useState('')
    return (
        <div className="assignModal">
            <div className="assignModalHeader">
                <span>Assign to Sales Agent</span>
            </div>
            <div className="assignModalBody">
                <Form.Select onChange={(e) => props.setSelectedSaleAgent(e.target.value)}>
                    <option value={null}>Select sales agent</option>
                    {
                        props?.listData?.map((item, index) => {
                            return (
                                <option value={item.id} key={index}>{item.name}</option>
                            )
                        })
                    }
                </Form.Select>

                <Form.Select style={{ marginTop: 10 }} onChange={(e) => props.setSelectedSaleAgent(e.target.value)}>
                    <option value={null}>Select projects</option>
                    {
                        props?.projects?.map((item, index) => {
                            return (
                                <option value={item.id} key={index}>{item.project_name}</option>
                            )
                        })
                    }
                </Form.Select>

                <Form.Group className="mb-12" controlId="comments" className="commentBox">
                    <Form.Control onChange={(e) => setComment(e.target.value)} aria-multiline type="text" name="comments" placeholder="Enter some comments for agent" />
                </Form.Group>
            </div>
            <div className="assignModalHeader row">
                <div className="col-sm-8">
                    <button className="btn btn-warning" onClick={() => props.setShowModal(!props.showModal)}>
                        Create New Project
                    </button>
                </div>
                <div className="col-sm-4">
                    <button className="btn btn-danger" onClick={() => props.setShowModal(!props.showModal)}>Cancel</button>
                    <button className="btn btn-warning" onClick={() => props.onSubmit(comment)}>Assign</button>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_contacts: state.main.get_contacts
});
export default connect(mapStateToProps, {})(PopUpModal);

