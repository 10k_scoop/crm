import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faEllipsisV, faEye, faSearch, faTrash } from "@fortawesome/free-solid-svg-icons";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";

const CustomTable = (props) => {

    return (
        <>
            <div className="createBranchBtn">
                <Link to="/createBranch">
                    <button className="btn btn-primary">{props.title}</button>
                </Link>
            </div>
            <div className="table-responsive">
                <Table striped bordered hover className="companiesTable">
                    <thead>
                        <tr>
                            <th className="searchIcon">
                                <FontAwesomeIcon icon={faSearch} />
                            </th>
                            <th>
                                <input
                                    name="name"
                                    autoComplete="off"
                                    onChange={props.onChange}
                                    placeholder="Name"
                                    type="text"
                                    className="filterInput" />
                            </th>
                            <th>
                                <input
                                    name="name"
                                    autoComplete="off"
                                    onChange={props.onChange}
                                    placeholder="Address"
                                    type="text"
                                    className="filterInput" />
                            </th>
                            <th>
                                <input
                                    name="name"
                                    autoComplete="off"
                                    onChange={props.onChange}
                                    placeholder="Company"
                                    type="text"
                                    className="filterInput" />
                            </th>
                            <th>
                                <input
                                    name="name"
                                    autoComplete="off"
                                    onChange={props.onChange}
                                    placeholder="Filter By date"
                                    type="date"
                                    className="filterInput" />
                            </th>
                            <th>
                                <button className="btn btn-secondary resetBtn">reset filter</button>
                            </th>

                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            props.data?.map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{item.id}</td>
                                        <td>{item.branch_name}</td>
                                        <td>{item.email}</td>
                                        <td><p>{item.address}</p></td>
                                        <td>{item.phone}</td>
                                        <td>
                                            <FontAwesomeIcon icon={faEye} className="tableActionsIcon" />
                                            <Link to={{ pathname: "/editBranch", state: { data: item } }}>
                                                <FontAwesomeIcon icon={faEdit} className="tableActionsIcon" />
                                            </Link>
                                            <FontAwesomeIcon icon={faTrash} className="tableActionsIcon"
                                                onClick={() => props.onDelete(item.id)} />
                                        </td>
                                    </tr>
                                )
                            })
                        }


                    </tbody>
                </Table>
            </div>
        </>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    auth: state.auth.get_recover_password_link,
});
export default connect(mapStateToProps, {})(CustomTable);

