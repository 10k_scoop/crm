import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faEllipsisV, faEye, faRefresh, faSearch, faTrash } from "@fortawesome/free-solid-svg-icons";
import { Form, Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import { role, role as userRole } from "../../../constants/CookiesData";

const GlobalTable = (props) => {

    const [selectedCheckBox, setSelectedCheckBox] = useState([])
    const [taskId, setTaskId] = useState([])
    const [isCheckedAll, setIsCheckedAll] = useState(false);

    const onCheckBoxPress = async (id, status, taskId) => {
        userRole() == "SalesAgent" ? setTaskId(taskId) : setTaskId([])
        if (id == "checkBoxAll") {
            if (!isCheckedAll) {
                props.data.map((item, index) => {
                    if (selectedCheckBox.includes(item.id)) {
                    } else {
                        setSelectedCheckBox((prevState) => ([...prevState, item.id]));
                    }
                })
            } else {
                setSelectedCheckBox([])
            }
        } else {
            if (!status) {
                setSelectedCheckBox((prevState) => ([...prevState, id]))
            } else {
                let arr = selectedCheckBox;
                let index = arr.indexOf(parseInt(id))
                arr.splice(index, 1)
                await setSelectedCheckBox([])
                setSelectedCheckBox(arr)
            }
            if (selectedCheckBox.length == 0) {
                setIsCheckedAll(false)
            }
        }
    }

    const onActionSelect = async (type) => {
        if (type != "Admin") {
            let comment = await prompt("Add any comment ")
            if (comment == null) {
                props.onAssignClick(selectedCheckBox, type, taskId, "")
                setSelectedCheckBox([])
                setTaskId([])
            } else {
                props.onAssignClick(selectedCheckBox, type, taskId, comment)
                setSelectedCheckBox([])
                setTaskId([])
            }
        } else {
            props.onAssignClick(selectedCheckBox, type, taskId, "")
            setSelectedCheckBox([])
            setTaskId([])
        }


    }
    //console.log(selectedCheckBox)

    return (
        <>
            <div className="globalTableBody">
                {/* Table header */}
                <div className="globalTableHeader">
                    <div className="row globalTableRow" >
                        <div className="col-sm-8">
                            <div className="tableTitle">{selectedCheckBox.length > 0 ? 'Selected ' + selectedCheckBox.length : props.title}</div>
                        </div>
                        {userRole() == "Admin" &&
                            <div className="col-sm-1 tableFilterCol">
                                {selectedCheckBox.length > 0 &&
                                    <>
                                        {props.assign &&
                                            <div className="btn btn-warning actionDoneBtn"
                                                onClick={() => onActionSelect("Admin")}
                                            >
                                                Assign
                                            </div>
                                        }
                                    </>
                                }

                            </div>
                        }
                        {userRole() == "SalesAgent" &&
                            <div className="col-sm-1 tableFilterCol">
                                {selectedCheckBox.length > 0 &&
                                    <>
                                        {props.leadStatus &&
                                            <div className="btn btn-warning actionDoneBtn"
                                                onClick={() => onActionSelect("SalesAgent")}
                                            >
                                                Change
                                            </div>
                                        }
                                    </>
                                }
                            </div>
                        }
                        <div className="col-sm-3 tableFilterCol">
                            <div className="filterTitle">Search</div>
                            <Form.Control type="text" onChange={(e) => props.onSearch(e.target.value)} placeholder="Search" className="filterInput" />
                        </div>
                    </div>
                </div>
                {/* Table header */}
                <div className="globalTableColumnHead">
                    {/* Table Column Names */}
                    <div className="globalTableRow" >
                        <table style={{ width: '100%', overflow: 'scroll' }} className="table-responsive">
                            <tbody>
                                <tr className="GlobalTableRowTr tr1">
                                    <td>
                                        <input
                                            checked={isCheckedAll}
                                            type="checkbox"
                                            name="checkBoxAll"
                                            onChange={(e) => {
                                                onCheckBoxPress('checkBoxAll')
                                                setIsCheckedAll(!isCheckedAll)
                                            }
                                            }
                                        />
                                    </td>
                                    {props?.col?.map((item, index) => {
                                        return (
                                            <td key={index}>
                                                {item.label}
                                            </td>
                                        )
                                    })}
                                    <td>Actions</td>
                                </tr>
                                {
                                    props?.data?.map((item, index) => {
                                        if (index >= 8) {
                                            return
                                        }
                                        return (
                                            <tr key={index} className="GlobalTableRowTr">
                                                {/* Table Rows */}
                                                <td id="checboxTd" style={{ borderColor: 'white !important' }}>
                                                    <input
                                                        checked={selectedCheckBox.includes(item.id) ? true : false}
                                                        type="checkbox"
                                                        name={'item.id'}
                                                        onChange={(e) => onCheckBoxPress(item.id, selectedCheckBox.includes(item.id) ? true : false, item?.agent_task?.id)}

                                                    />
                                                </td>
                                                {
                                                    props.col.map((data, index2) => {
                                                        if (typeof (item[data.name]) == "object") {

                                                            if (data.name == "branches") {
                                                                return (
                                                                    <td key={index2}>
                                                                        {
                                                                            item[data.name].map((item3, index3) => {
                                                                                return item3.branch_name
                                                                            })
                                                                        }
                                                                    </td>
                                                                )
                                                            } else if (data.name == "roles") {
                                                                return (
                                                                    <td key={index2}>
                                                                        {
                                                                            item[data.name].map((item3, index3) => {
                                                                                return (

                                                                                    <button className="btn btn-warning" key={index}>
                                                                                        {item3.name}
                                                                                    </button>
                                                                                )
                                                                            })
                                                                        }
                                                                    </td>
                                                                )
                                                            } else if (data.name == "permissions") {
                                                                // console.log(item)
                                                                return (
                                                                    <td key={index2}>
                                                                        {
                                                                            item.permissions?.map((item3, index3) => {
                                                                                return (
                                                                                    <button key={index3} className="btn btn-warning" style={{ marginRight: 5 }}>
                                                                                        {item3.name}
                                                                                    </button>
                                                                                )
                                                                            })
                                                                        }
                                                                    </td>
                                                                )
                                                            } else if (data.name == "tasks" || data.name == "agent_task") {
                                                                if (userRole() == "Admin" || userRole() == "SuperAdmin" || props.title == "Contacts") {
                                                                    return (
                                                                        <td key={index2}>
                                                                            <span style={{ color: 'green', fontWeight: '500' }}>
                                                                                {
                                                                                    data.name == "tasks" ?
                                                                                        item?.tasks?.leadstatus?.name :
                                                                                        item?.agent_task?.leadstatus?.name
                                                                                }
                                                                            </span>
                                                                        </td>
                                                                    )
                                                                } else if (userRole() == "SalesAgent" && props.title != "Contacts") {
                                                                    return (
                                                                        <td key={index2} style={{ padding: 10 }}>
                                                                            <Form.Select
                                                                                defaultValue={item?.agent_task?.leadstatus.id}
                                                                                style={{ fontSize: 12, width: 150 }}
                                                                                onChange={(e) => props.setLeadStatusDropDown(e.target.value)}
                                                                            >
                                                                                <option value={null}>Select</option>
                                                                                {
                                                                                    props.leadStatusList?.map((item, index) => {
                                                                                        return (
                                                                                            <option key={index} value={item.id}>{item.name}</option>
                                                                                        )
                                                                                    })
                                                                                }
                                                                            </Form.Select>
                                                                        </td>
                                                                    )
                                                                } else {
                                                                }


                                                            } else if (data.name == "landing_page_link") {
                                                                //console.log(data.name)
                                                                return (
                                                                    <td key={index2}>
                                                                        none
                                                                    </td>
                                                                )
                                                            }
                                                            else {
                                                                return (
                                                                    <td key={index2}>
                                                                        --
                                                                    </td>
                                                                )
                                                            }
                                                        }
                                                        else {
                                                            if (data.name == "users") {
                                                                return <td key={index2}>
                                                                    {
                                                                        role() == "SalesAgent" ?
                                                                            item?.agent_task?.users?.name :
                                                                            item?.tasks?.users?.name
                                                                    }
                                                                </td>

                                                            } if (data.name == "comments") {
                                                                return <td key={index2}>
                                                                    {
                                                                        role() == "SalesAgent" ?
                                                                            item?.agent_task?.comments :
                                                                            item?.tasks?.comments
                                                                    }
                                                                </td>
                                                            } else {
                                                                console.log(item[data.name])
                                                                return (
                                                                    <td key={index2}>{item[data.name] != null ? item[data.name] : "global Empty"}
                                                                    </td>
                                                                )
                                                            }
                                                        }

                                                    })

                                                }
                                                <td>
                                                    {props.showEditBtn &&
                                                        <Link to={{ pathname: props.editPathname, state: { data: item, Hpermissions: props.permissions } }}>
                                                            <FontAwesomeIcon icon={faEdit} className="tableActionsIcon" />
                                                        </Link>
                                                    }
                                                    {props.showDeleteBtn &&
                                                        <FontAwesomeIcon icon={faTrash} className="tableActionsIcon"
                                                            onClick={() => props.onDelete(item.id)} />
                                                    }
                                                    {role() == "Admin" || role() == "SuperAdmin" ?
                                                        <Link to={{ pathname: props.viewPathname, state: { data: item } }}>
                                                            <FontAwesomeIcon
                                                                icon={faEye}
                                                                className="tableActionsIcon"
                                                            />
                                                        </Link>
                                                        :
                                                        <FontAwesomeIcon
                                                            icon={faEye}
                                                            className="tableActionsIcon"
                                                        />
                                                    }
                                                    {role() == "SuperAdmin" && props.edit != "noEdit" ?
                                                        <>
                                                            <Link to={{ pathname: props.editPathname, state: { data: item, Hpermissions: props.permissions } }}>
                                                                <FontAwesomeIcon icon={faEdit} className="tableActionsIcon" />
                                                            </Link>
                                                            <FontAwesomeIcon icon={faTrash} className="tableActionsIcon"
                                                                onClick={() => props.onDelete(item.id)} />
                                                        </>
                                                        :
                                                        null
                                                    }
                                                </td>
                                                {/* Table rows */}
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        </table>

                    </div>
                    {/* Table Column Names */}
                </div>
                {/* Table header */}
            </div>
        </>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
});
export default connect(mapStateToProps, {})(GlobalTable);

