export const ContactsColumnSalesAgent = [
    { name: 'company_name', label: 'Company Name' },
    { name: 'email', label: 'Email' },
    { name: 'first_name', label: 'First Name' },
    { name: 'last_name', label: 'Last Name' },
    { name: 'job_title', label: 'Job Title' },
    { name: 'agent_task', label: 'Lead Status' },
    { name: 'mobile_phone_number', label: 'Phone' },
    { name: 'landing_page_link', label: 'Website' },
    { name: 'no_of_repeats', label: 'Repeat' },
    { name: 'street_address', label: 'Address' },
    { name: 'updated_at', label: 'Updated At' },
    { name: 'comments', label: 'Comment' }, 
]
export const ProjectsColumn = [
    { name: 'id', label: 'No.' },
    { name: 'project_name', label: 'Project Name' },
    { name: 'description', label: 'Description' },
    { name: 'status', label: 'Status ' },
    { name: 'type', label: 'Type' },
]

export const ContactsColumn = [
    { name: 'company_name', label: 'Company Name' },
    { name: 'email', label: 'Email' },
    { name: 'first_name', label: 'First Name' },
    { name: 'last_name', label: 'Last Name' },
    { name: 'job_title', label: 'Job Title' },
    { name: 'tasks', label: 'Lead Status' },
    { name: 'mobile_phone_number', label: 'Phone' },
    { name: 'landing_page_link', label: 'Website' },
    { name: 'no_of_repeats', label: 'Repeat' },
    { name: 'street_address', label: 'Address' },
    { name: 'updated_at', label: 'Updated At' },
    { name: 'users', label: 'Assigned to' },
    { name: 'comments', label: 'Comment' },
]

export const CustomersColumn = [
    { name: 'name', label: 'Name' },
    { name: 'email', label: 'Email' },
    { name: 'phone', label: 'Phone' },
    { name: 'location', label: 'Location' },
    { name: 'landing_page_link', label: 'Website' },
    { name: 'created_at', label: 'Created At' },
]

export const UsersColumn = [
    { name: 'name', label: 'Name' },
    { name: 'email', label: 'Email' },
    { name: 'branches', label: 'Branch' },
    { name: 'roles', label: 'Role' },
]

export const BranchColumn = [
    { name: 'id', label: 'Id' },
    { name: 'branch_name', label: 'Branch Name' },
    { name: 'email', label: 'Email' },
    { name: 'address', label: 'Address' },
    { name: 'phone', label: 'Phone' },
]

export const PermissionsColumn = [
    { name: 'id', label: 'Id' },
    { name: 'name', label: 'Name' },
    { name: 'permissions', label: 'Permissions' }
]

export const propertyOwnersColumn = [
    { name: 'id', label: 'Id' },
    { name: 'name', label: 'Name' },
    { name: 'email_address', label: 'Email' },
    { name: 'contact', label: 'Contact' },
    { name: 'emirates_id', label: 'Emirates Id' },
    { name: 'nationality', label: 'Nationality' },
    { name: 'passport', label: 'Passport' },
    { name: 'passport_number', label: 'Passport No' }
]

export const propertyColumn = [
    { name: 'id', label: 'Id' },
    { name: 'tower_name', label: 'Tower Name' },
    { name: 'unit_number', label: 'Unit no' },
    { name: 'floor_number', label: 'Floor no' },
    { name: 'parking_slot', label: 'Parking Slot' },
    { name: 'number_of_bedrooms', label: 'No of bedrooms' },
    { name: 'kitchen', label: 'Kitchen' },
    { name: 'hall', label: 'Hall' },
    { name: 'furnished', label: 'Furnished' },
    { name: 'status', label: 'Status' },
    { name: 'description', label: 'Description' },
]

export const TenantColumn = [
    { name: 'id', label: 'Id' },
    { name: 'name', label: 'Name' },
    { name: 'email_address', label: 'Email' },
    { name: 'contact', label: 'Contact' },
    { name: 'emirates_id', label: 'Emirates Id' },
    { name: 'nationality', label: 'Nationality' },
    { name: 'passport', label: 'Passport' },
    { name: 'passport_number', label: 'Passport No' },
    { name: 'emirates_id_file', label: 'Emirated id file' }
]

export const EmployeeColumn = [
    { name: 'id', label: 'Id' },
    { name: 'name', label: 'Name' },
    { name: 'email_address', label: 'Email' },
    { name: 'contact', label: 'Contact' },
    { name: 'emirates_id', label: 'Emirates Id' },
    { name: 'nationality', label: 'Nationality' },
    { name: 'passport', label: 'Passport' },
    { name: 'passport_number', label: 'Passport No' },
    { name: 'emirates_id_file', label: 'Emirated id file' },
    { name: 'joining_date', label: 'Join Date' },
    { name: 'visa_number', label: 'VN' },
    { name: 'visa_expiry_date', label: 'VE' },
    { name: 'labour_contract_number', label: 'LCN' },
    { name: 'labour_contract_expiry', label: 'LCE' },
    { name: 'visa', label: 'Visa' },
    { name: 'labour_contract', label: 'LC' },
    { name: 'photo', label: 'Photo' },
]

export const TasksColumn = [
    { name: 'id', label: 'Id' },
    { name: 'branch_id', label: 'Branch Id' },
    { name: 'assigned_by_id', label: 'Assigned by' },
    { name: 'assigned_to_id', label: 'Assigned to' },
    { name: 'contact_id', label: 'Contact Id' },
    { name: 'status', label: 'Status' },
    { name: 'comment', label: 'Comment' },
]

export const CallLogsColumn = [
    { name: 'call_title', label: 'Title' },
    { name: 'activity_date', label: 'Activity Date' },
    { name: 'call_notes', label: 'Notes' },
    { name: 'call_outcome', label: 'Outcome' },
    { name: 'transcript_available', label: 'Transcript' },
    { name: 'call_duration', label: 'Call Duration' },
]
export const TenancyContractsColumn = [
    { name: 'call_title', label: 'Title' },
    { name: 'activity_date', label: 'Activity Date' },
    { name: 'call_notes', label: 'Notes' },
    { name: 'call_outcome', label: 'Outcome' },
    { name: 'transcript_available', label: 'Transcript' },
    { name: 'call_duration', label: 'Call Duration' },
]

export const AttendanceColumn = [
    { name: 'date', label: 'Date' },
    { name: 'total_hours', label: 'Total Hours' },
    { name: 'employee_id', label: 'Employee Id' },
    { name: 'updated_at', label: 'Updated At' },
]