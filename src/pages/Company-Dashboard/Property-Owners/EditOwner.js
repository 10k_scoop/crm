import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { editPropertyOwner } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import Select from 'react-select'
import { useLocation } from "react-router";

const EditOwner = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const [name, setName] = useState()
    const [permissions, setPermissions] = useState([])
    const location = useLocation()
    const { data } = location.state
    const [ownerData, setOwnerData] = useState(
        {
            name: data.name,
            email_address: data.email_address,
            contact: data.contact,
            passport_number: data.passport_number,
            passport:data.passport,
            emirates_id: data.emirates_id,
            nationality: data.nationality,
            emirates_id_file: data.emirates_id_file,
            created_at: new Date(),
            id:data.id
        }
    );

    const onCreate = (e) => {
        setLoading(true)
        e.preventDefault()
        props.editPropertyOwner(ownerData, setLoading)
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setOwnerData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };


    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Edit Contact</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control value={ownerData.name} onChange={(e) => onChange(e)} type="text" required name="name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Email</Form.Label>
                                <Form.Control value={ownerData.email_address} onChange={(e) => onChange(e)} type="email" required name="email_address" />
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Contact</Form.Label>
                                <Form.Control value={ownerData.contact} onChange={(e) => onChange(e)} type="text" required name="contact" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Passport no</Form.Label>
                                <Form.Control value={ownerData.passport_number} onChange={(e) => onChange(e)} type="text" required name="passport_number" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Emirates Id</Form.Label>
                                <Form.Control value={ownerData.emirates_id} onChange={(e) => onChange(e)} type="text" required name="emirates_id" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Nationality</Form.Label>
                                <Form.Control value={ownerData.nationality} onChange={(e) => onChange(e)} type="text" required name="nationality" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Passport</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="file" name="passport" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Emirate Id</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="file" name="emirates_id_file" />
                            </Form.Group>
                        </div>
                    </div>

                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Save
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    edit_property_owner: state.main.edit_property_owner
});
export default connect(mapStateToProps, { editPropertyOwner })(EditOwner);

