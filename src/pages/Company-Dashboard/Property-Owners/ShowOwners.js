import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/CompanyDashboard.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNav from "../components/SideNav";
import { showPropertyOwners, deletePropertyOwner } from "../../../state-management/actions/Features";
import Header from "../components/Header";
import GlobalTable from "../components/GlobalTable";
import { Link } from "react-router-dom";
import { propertyOwnersColumn } from "../components/TableColumn";


const ShowOwners = (props) => {

    const [loading, setLoading] = useState(true)
    const [showSideBar, setShowSideBar] = useState(true)
    const [propertyOwners, setPropertyOwners] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);

    useEffect(() => {
        props.showPropertyOwners(setLoading)
    }, [])

    useEffect(() => {
        if (props.get_property_owners != null) {
            setPropertyOwners(props.get_property_owners)
        } else {
            console.log(props.get_property_owners)
        }
    }, [props])

     //Search
    const onSearch = (val, type) => {
        let target = val.toLowerCase();
        let filteredData = propertyOwners.filter(function (item) {
            return item.name.toLowerCase().includes(val)
        });

        setfilteredData(filteredData);
    };


    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar}/>
            {/* SideMenu */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}

                {/* dashboard Body */}
                <div className="DashboardMiddleContent container">
                    <h2 style={{ marginBottom: 10 }}>Property Owners</h2>
                    {/* Table */}
                    <Link to="/createPropertyOwner" >
                        <button className="btn btn-primary">Create Property Owner</button>
                    </Link>
                    <GlobalTable
                        title="Property Owners"
                        onSearch={(val) => {
                            onSearch(val)
                            setSearchText(val)
                        }}
                        onDelete={(id) => props.deletePropertyOwner(id, setLoading)}
                        onRefresh={() => {
                            setLoading(true)
                            props.showPropertyOwners(setLoading)
                        }}
                        editPathname="/editPropertyOwner"
                        actions={["Delete", "View", "Edit"]}
                        col={propertyOwnersColumn}
                        data={searchText == "" ? propertyOwners : filteredData}
                    />
                    {/* Table */}
                </div>
                {/* dashboard Body */}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_property_owners: state.main.get_property_owners
});
export default connect(mapStateToProps, { deletePropertyOwner, showPropertyOwners })(ShowOwners);

