import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { createUser } from "../../../state-management/actions/Features";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import { getMyBranches } from "../../../state-management/actions/auth/authActions";

const CreateUser = (props) => {

    const [loading, setLoading] = useState(true)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const [branches, setBranches] = useState([])
    const [userData, setUserData] = useState(
        {
            name: "",
            email: "",
            branch_id: null,
            password: "",
            confirm_password: "",
            created_at: new Date(),

        }
    );

    const onCreate = (e) => {
        if (userData.password == userData.confirm_password) {
            setLoading(true)
            e.preventDefault()
            props.createUser(userData, setLoading)
        } else {
            alert("Password doesnot match !")
        }
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setUserData((prevState) => ({
            ...prevState,
            [name]: value,
        }));

    };

    useEffect(() => {
        props.getMyBranches(setLoading)
    }, [])

    useEffect(() => {
        if (props.get_branches != null) {
            setBranches(props.get_branches[0].branches)
        }
    }, [props])

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav
                status={showSideBar}
            />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Create User</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="text" required name="name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="email" required name="email" />
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="password" required name="password" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="cpassword">
                                <Form.Label>Confirm Password</Form.Label>
                                <Form.Control onChange={(e) => onChange(e)} type="password" required name="confirm_password" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="branches">
                                <Form.Label>Branch</Form.Label>
                                <Form.Select onChange={(e) => onChange(e)} type="text" required name="branch_id" >
                                    <option value={null}>select branch</option>
                                    {
                                        branches?.map((item, index) => {
                                            console.log(item)
                                            return (
                                                <option key={index} value={item.id}>{item.branch_name}</option>
                                            )
                                        })
                                    }
                                </Form.Select>
                            </Form.Group>
                        </div>
                    </div>

                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Create
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    create_user: state.main.create_user,
    get_branches: state.main.get_branches,

});
export default connect(mapStateToProps, { createUser, getMyBranches })(CreateUser);

