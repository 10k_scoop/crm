import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/CompanyDashboard.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNav from "../components/SideNav";
import { showUsers, deleteUser } from "../../../state-management/actions/Features";
import Header from "../components/Header";
import GlobalTable from "../components/GlobalTable";
import { UsersColumn } from "../components/TableColumn";
import { Link } from "react-router-dom";
import { role } from "../../../constants/CookiesData";

const ShowUsers = (props) => {

    const [loading, setLoading] = useState(true)
    const [showSideBar, setShowSideBar] = useState(true)
    const [users, setUsers] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);

    useEffect(() => {
        props.showUsers(setLoading)
    }, [])

    useEffect(() => {
        if (props.get_users != null) {
            setUsers(props.get_users)
            setfilteredData(props.get_users)
        } else {
            console.log(props)
        }
    }, [props])

    //Search
    const onSearch = (val, type) => {
        let filteredData = users.filter(function (item) {
            let name = item.name.toLowerCase().includes(val)
            let branch_name = item.branches[0].branch_name.includes(val)
            if (name != "") {
                return name
            }
            else {
                return branch_name
            }
        });
        setfilteredData(filteredData);
        console.log(val)
    };

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav
                status={showSideBar}
            />
            {/* SideMenu */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}

                {/* dashboard Body */}
                <div className="DashboardMiddleContent container">
                    <h2 style={{ marginBottom: 10 }}>Users</h2>
                    <Link to={role()=="Admin"?"/createUserWithRole":"/createUser"}>
                        <button className="btn btn-primary">Create User</button>
                    </Link>
                    {/* Table */}
                    <GlobalTable
                        title="Users"
                        onSearch={(val) => {
                            console.log(val)
                            onSearch(val)
                            setSearchText(val)
                        }}
                        onDelete={(id) => props.deleteUser(id, setLoading)}
                        onRefresh={() => {
                            setLoading(true)
                            props.showUsers(setLoading)
                        }}
                        editPathname="/editUser"
                        actions={["Delete", "View", "Edit"]}
                        col={UsersColumn}
                        data={searchText == "" ? users : filteredData}
                    />
                    {/* Table */}
                </div>
                {/* dashboard Body */}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_users: state.main.get_users
});
export default connect(mapStateToProps, { deleteUser, showUsers })(ShowUsers);

