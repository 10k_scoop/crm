import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { editUser } from "../../../state-management/actions/Features";
import { getMyBranches } from "../../../state-management/actions/auth/authActions";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import { useLocation } from "react-router";

const EditUser = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const [branches, setBranches] = useState([])
    const location = useLocation()
    const { data } = location.state
    const [userData, setUserData] = useState(
        {
            id:data?.id,
            name: data?.name,
            email: data?.email,
            branches: data?.branches,
            created_at: new Date(),
            branch_id:data.branches[0].id
        }
    );

    useEffect(() => {
        props.getMyBranches(setLoading)
    }, [])

    useEffect(() => {
        if (props.get_branches != null) {
            setBranches(props.get_branches[0].branches)
        } else {
            console.log(props)
        }
    }, [props])

    const onCreate = (e) => {
        setLoading(true)
        e.preventDefault()
        props.editUser(userData, setLoading)
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setUserData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };


    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav
                status={showSideBar}
            />
            {/* SideMenu */}
            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Edit User</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control defaultValue={userData?.name} onChange={(e) => onChange(e)} type="text" required name="name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control defaultValue={userData?.email} onChange={(e) => onChange(e)} type="email" required name="email" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="branche_id">
                                <Form.Label>Branch</Form.Label>
                                <Form.Select onChange={(e) => onChange(e)} type="text" required name="branche_id" >
                                    {
                                        branches.map((item, index) => {
                                            if (item.id == userData.branches[0].id) {
                                                return (
                                                    <option
                                                        selected
                                                        key={index}>
                                                        {item.branch_name}
                                                    </option>
                                                )
                                            } else {
                                                return (
                                                    <option

                                                        key={index}>
                                                        {item.branch_name}
                                                    </option>
                                                )
                                            }
                                        })
                                    }
                                </Form.Select>
                            </Form.Group>
                        </div>
                    </div>

                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Create
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    edit_user: state.main.edit_user,
    get_branches: state.main.get_branches,
});
export default connect(mapStateToProps, { editUser, getMyBranches })(EditUser);

