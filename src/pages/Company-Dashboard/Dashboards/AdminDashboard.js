import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { showRoles, deleteRole } from "../../../state-management/actions/Features";
import Cookies from "js-cookie";
import { Spinner } from "react-bootstrap";
import { showUsers } from "../../../state-management/actions/Features";
import ReportCards from "../components/ReportCards";
import GlobalTable from "../components/GlobalTable";
import { UsersColumn } from "../components/TableColumn";
import { faChartColumn, faChartLine, faChartPie } from "@fortawesome/free-solid-svg-icons";

const AdminDashboard = (props) => {

    const [loading, setLoading] = useState(false)
    const [showSideBar, setShowSideBar] = useState(true)
    let userDetail = Cookies.get('userDetails')
    const [users, setUsers] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);

    useEffect(() => {
        if (props.get_users != null) {
            setUsers(props.get_users)
            setfilteredData(props.get_users)
        } else {
            console.log(props)
        }
    }, [props])

    useEffect(() => {
        props.showUsers(setLoading)
    }, [])


    //Search
    const onSearch = (val, type) => {
        let filteredData = users.filter(function (item) {
            let name = item.name.toLowerCase().includes(val)
            let branch_name = item.branches[0].branch_name.includes(val)
            if (name != "") {
                return name
            }
            else {
                return branch_name
            }
        });
        setfilteredData(filteredData);
        console.log(val)
    };

    return (
        <>
            {/* Loader */}
            {loading &&
                <div className="freeLoader">
                    <Spinner animation="grow" size="lg" color="blue" />
                    <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                </div>
            }
            <div className="DashboardMiddleContent container">
                <h2 style={{ marginBottom: 10 }}>Admin</h2>
                <div className="row">
                    <ReportCards title="USERS" num={props.get_users?.length} icon={faChartPie} />
                    <ReportCards title="CONTACTS" num="33" icon={faChartLine} />
                    <ReportCards title="PROPERTIES" num="101" icon={faChartColumn} />
                </div>
                <div className="usersTable col-sm-12">
                    <GlobalTable
                        title="Users"
                        onSearch={(val) => {
                            console.log(val)
                            onSearch(val)
                            setSearchText(val)
                        }}
                        onDelete={(id) => props.deleteUser(id, setLoading)}
                        onRefresh={() => {
                            setLoading(true)
                            props.showUsers(setLoading)
                        }}
                        actions={["Delete", "View", "Edit"]}
                        col={UsersColumn}
                        data={searchText == "" ? users : filteredData}
                    />
                </div>
            </div>
        </>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_users: state.main.get_users
});
export default connect(mapStateToProps, { showUsers })(AdminDashboard);

