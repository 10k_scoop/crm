import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Cookies from "js-cookie";
import { Spinner } from "react-bootstrap";
import { showContacts, showCustomers, updateTask } from "../../../state-management/actions/Features";
import ReportCards from "../components/ReportCards";
import GlobalTable from "../components/GlobalTable";
import { faChartColumn, faChartLine, faChartPie } from "@fortawesome/free-solid-svg-icons";
import { ContactsColumn, ContactsColumnSalesAgent, TasksColumn } from "../components/TableColumn";
import { role } from "../../../constants/CookiesData";
const SalesAgentDashboard = (props) => {

    const [loading, setLoading] = useState(false)
    const [showSideBar, setShowSideBar] = useState(true)
    const [tasks, setTasks] = useState([])
    const [leadStatusList, setLeadStatusList] = useState([])
    const [searchText, setSearchText] = useState("");
    const [leadStatusDropDown, setLeadStatusDropDown] = useState("");
    const [filteredData, setfilteredData] = useState([]);

    useEffect(() => {
        if (props.get_contacts != null) {
            setTasks(props.get_contacts)
            setfilteredData(props.get_contacts)
            setLeadStatusList(props.get_contacts?.leadstatus)
        } else {
            //  console.log(props)
        }
    }, [props])
    useEffect(() => {
        props.showContacts(setLoading)
        props.showCustomers(setLoading)
    }, [])


    //Search
    const onSearch = (val, type) => {
        let filteredData = tasks.filter(function (item) {
            let name = item.name.toLowerCase().includes(val)
            let branch_name = item.branches[0].branch_name.includes(val)
            if (name != "") {
                return name
            }
            else {
                return branch_name
            }
        });
        setfilteredData(filteredData);
    };

    const LeadUpdate = (id, type, taskId,comment) => {
        setLoading(true)
        let data = {
            lead_status_id: leadStatusDropDown,
            comments:comment
        }
       props.updateTask(taskId, data, setLoading)
    }


    return (
        <>
            {/* Loader */}
            {loading &&
                <div className="freeLoader">
                    <Spinner animation="grow" size="lg" color="blue" />
                    <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                </div>
            }
            <div className="DashboardMiddleContent">
                <h2 style={{ marginBottom: 10 }}>Sales Agent</h2>
                <div className="row container">
                    <ReportCards title="TASKS" num={props.get_contacts?.data.length} icon={faChartPie} />
                    <ReportCards title="CONTACTS" num={props.get_contacts?.data.length} icon={faChartLine} />
                    <ReportCards title="CUSTOMERS" num={props.get_customers?.length} icon={faChartColumn} />
                </div>
                <div className="usersTable col-sm-12">
                    <GlobalTable
                        setLeadStatusDropDown={setLeadStatusDropDown}
                        leadStatus={true}
                        title="Tasks"
                        onSearch={(val) => {
                            console.log(val)
                            onSearch(val)
                            setSearchText(val)
                        }}
                        onAssignClick={(e, type, taskId,comment) => LeadUpdate(e, '', taskId,comment)}
                        onDelete={(id) => props.deleteUser(id, setLoading)}
                        onRefresh={() => {
                            setLoading(true)
                            props.showUsers(setLoading)
                        }}
                        actions={["Delete", "View", "Edit"]}
                        col={ContactsColumnSalesAgent}
                        fullData={tasks}
                        leadStatusList={leadStatusList}
                        data={searchText == "" ? tasks.data
                            : filteredData}
                    />
                </div>
            </div>
        </>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_contacts: state.main.get_contacts,
    get_customers: state.main.get_customers
});
export default connect(mapStateToProps, { showContacts, updateTask, showCustomers })(SalesAgentDashboard);

