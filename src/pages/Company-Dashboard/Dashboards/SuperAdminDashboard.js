import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { getMyBranches, deleteCompanyBranch } from "../../../state-management/actions/auth/authActions";
import { showRoles, deleteRole } from "../../../state-management/actions/Features";
import { Spinner } from "react-bootstrap";
import { Link } from "react-router-dom";
import GlobalTable from "../components/GlobalTable";
import { BranchColumn } from "../components/TableColumn";

const SuperAdminDashboard = (props) => {

    const [loading, setLoading] = useState(true)
    const [branches, setBranches] = useState([])
    const [searchText, setSearchText] = useState("");
    const [filteredData, setfilteredData] = useState([]);

    useEffect(() => {
        props.getMyBranches(setLoading)
    }, [])

    useEffect(() => {
        if (props.get_branches != null) {
            setBranches(props.get_branches[0].branches)
            setfilteredData(props.get_branches[0].branches)
        } else {
            console.log(props)
        }
    }, [props])

    //onRefreshPage
    const onRefreshPage = () => {
        setLoading(true)
        props.getMyBranches(setLoading)

    }
    //Search
    const onSearch = (val, type) => {
        let filteredData = branches.filter(function (item) {
            let name = item.branch_name.toLowerCase().includes(val)
            if (name != "") {
                return name
            }
            else {
                return ''
            }
        });
        setfilteredData(filteredData);
        console.log(val)
    };

    return (
        <>
            {/* Loader */}
            {loading &&
                <div className="freeLoader">
                    <Spinner animation="grow" size="lg" color="blue" />
                    <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                </div>
            }
            <div className="DashboardMiddleContent container">
                <h2 style={{ marginBottom: 10 }}>Branches</h2>
                <Link to="/createBranch">
                    <button className="btn btn-primary">Create Branch</button>
                </Link>
                <GlobalTable
                    title="Branches"
                    onSearch={(val) => {
                        onSearch(val)
                        setSearchText(val)
                    }}
                    onDelete={(id) => props.deleteCompanyBranch(id, setLoading)}
                    onRefresh={() => {
                        setLoading(true)
                        props.getMyBranches(setLoading)
                    }}
                    editPathname="/editBranch"
                    actions={["Delete", "View", "Edit"]}
                    col={BranchColumn}
                    data={searchText == "" ? branches : filteredData}
                />
            </div>
        </>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_roles: state.main.get_roles
});
export default connect(mapStateToProps, { getMyBranches, deleteCompanyBranch, deleteRole, showRoles })(SuperAdminDashboard);

