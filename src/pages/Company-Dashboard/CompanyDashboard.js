import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../styles/Home.css";
import "../../styles/CompanyDashboard.css";
import "../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import SideNav from "./components/SideNav";
import CustomTable from "./components/CustomTable";
import { getMyBranches, deleteCompanyBranch } from "../../state-management/actions/auth/authActions";
import { showRoles, deleteRole } from "../../state-management/actions/Features";
import Header from "./components/Header";
import Cookies from "js-cookie";
import SuperAdminDashboard from "./Dashboards/SuperAdminDashboard";
import AdminDashboard from "./Dashboards/AdminDashboard";
import { role as userRole } from "../../constants/CookiesData";
import SalesAgentDashboard from "./Dashboards/SalesAgentDashboard";

const CompanyDashboard = (props) => {

    const [loading, setLoading] = useState(false)
    const [showSideBar, setShowSideBar] = useState(true)

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav
                status={showSideBar}
                onNavClose={()=>setShowSideBar(false)}
            />
            {/* SideMenu */}

            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}

                {/* dashboard Body */}
                <div className="DashboardMiddleContent container-fluid">
                    {
                        userRole() == "Admin" ?
                            <AdminDashboard />
                            : userRole() == "SalesAgent" ?
                                <SalesAgentDashboard />
                                : userRole() == "SuperAdmin" ?
                                    <SuperAdminDashboard />
                                    : null
                    }
                </div>
                {/* dashboard Body */}
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches,
    get_roles: state.main.get_roles
});
export default connect(mapStateToProps, { getMyBranches, deleteCompanyBranch, deleteRole, showRoles })(CompanyDashboard);

