import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../styles/Home.css";
import "../../../styles/Responsive.css";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner'
import { Form } from "react-bootstrap";
import { editCompanyBranch } from "../../../state-management/actions/auth/authActions";
import SideNav from "../components/SideNav";
import Header from "../components/Header";
import { useLocation } from 'react-router-dom'

const EditBranch = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [showSideBar, setShowSideBar] = useState(true)
    const location = useLocation()
    const { data } = location.state
    const [branchData, setBranchData] = useState(
        {
            branch_id: data?.id,
            email: data?.email,
            address: data?.address,
            branch_name: data?.branch_name,
            phone: data?.phone,
            zipcode: data?.zipcode
            
        }
    );

    const onCreate = (e) => {
        if (branchData.email != null && branchData.branch_name != null) {
            e.preventDefault()
            setLoading(true)
            props.editCompanyBranch(branchData, setLoading)
        }
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setBranchData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    return (
        <div className="companyDashboardWrapper">
            {/* SideMenu */}
            <SideNav status={showSideBar} />
            {/* SideMenu */}


            <div className="companyDashboardContent">
                {/* Loader */}
                {loading &&
                    <div className="freeLoader">
                        <Spinner animation="grow" size="lg" color="blue" />
                        <p style={{ marginLeft: 10, fontFamily: 'Montserrat' }}>Please wait...</p>
                    </div>
                }
                {/* Header Content */}
                <Header setShowSideBar={setShowSideBar} showSideBar={showSideBar} />
                {/* Header Content */}
                <div className="createBranchTitle">Edit Branch</div>
                <Form style={{ paddingRight: 20, paddingLeft: 20 }}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Branch Name</Form.Label>
                                <Form.Control value={branchData.branch_name} onChange={onChange} type="text" placeholder="Name" required name="branch_name" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control value={branchData.email} onChange={onChange} type="email" placeholder="Email" required name="email" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="address">
                                <Form.Label>Address</Form.Label>
                                <Form.Control value={branchData.address} onChange={onChange} type="text" placeholder="Address" name="address" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="zipCode">
                                <Form.Label>Zip Code</Form.Label>
                                <Form.Control value={branchData.zipcode} onChange={onChange} type="number" placeholder="Zip Code" name="zipcode" />
                            </Form.Group>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <Form.Group className="mb-3" controlId="phone">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control value={branchData.phone} onChange={onChange} type="number" placeholder="Phone" required name="phone" />
                            </Form.Group>
                        </div>
                        <div className="col-sm-6">

                        </div>
                    </div>
                    <button className="btn btn-primary" type="submit" onClick={onCreate}>
                        Save
                    </button>
                </Form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    get_branches: state.main.get_branches
});
export default connect(mapStateToProps, { editCompanyBranch })(EditBranch);

