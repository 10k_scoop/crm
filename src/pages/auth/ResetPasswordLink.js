import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../styles/Home.css";
import "../../styles/Responsive.css";
import InputField from "./components/InputField";
import { connect } from "react-redux";
import { resetNewPassword } from "../../state-management/actions/auth/authActions";
import Spinner from 'react-bootstrap/Spinner'

const ResetPasswordLink = (props) => {

    const [loading, setLoading] = useState(false)
    let getToken = window.location.pathname.split("/")[window.location.pathname.split("/").length - 2]
    let email = window.location.pathname.split("/")[window.location.pathname.split("/").length - 1]
    const [errorMsg, setErrorMsg] = useState("")
    const [passData, setPassData] = useState({ password: "", password_confirmation: "", email: email, token: getToken });

    const onReset = () => {
        if (passData.new_passowrd != "" && passData.password_confirmation != "") {
            if (passData.password == passData.password_confirmation) {
                setLoading(true)
                setErrorMsg("")
                props.resetNewPassword(passData, setLoading)
            } else {
                setErrorMsg("Password doesnot match!")
            }
        } else {
            setErrorMsg("Enter password")
        }
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setPassData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    useEffect(() => {
        if (props.auth?.success ==true) {
            setErrorMsg("Password Changed Successfully")
            window.location.href="/"
            console.log(props.auth)
        } else if (props.errors != null) {
            if (props.errors?.status == 401) {
                setErrorMsg("Wrong Email or Password !")
            } else {
                setErrorMsg("Something went wrong !")
            }
        }
    }, [props])

    return (
        <div className="loginBodyWrapper">

            <div className="formBodyWrapper">
                <div className="loginTitle">Set New Password</div>
                <div className="errorMsg">{errorMsg}</div>
                <InputField name="password" onChange={onChange} placeholder="New Password" type="password" />
                <InputField name="password_confirmation" onChange={onChange} placeholder="Confirm Password" type="password" />
                <div className="formBtnwrapper">
                    <div className="loginBtn" onClick={() => loading ? null : onReset()}>
                        {
                            loading ?
                                <Spinner animation="grow" size="sm" />
                                :
                                "Reset"
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    auth: state.auth.set_new_password,
});
export default connect(mapStateToProps, { resetNewPassword })(ResetPasswordLink);

