import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../styles/Home.css";
import "../../styles/Responsive.css";
import InputField from "./components/InputField";
import { connect } from "react-redux";
import { postLoginUser } from "../../state-management/actions/auth/authActions";
import Spinner from 'react-bootstrap/Spinner'
import { useHistory } from 'react-router-dom';
import { faEnvelope, faG, faLock } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Image } from "react-bootstrap";
import GoogleLogin from 'react-google-login';

const Login = (props) => {

    const history = useHistory();
    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [loginData, setLoginData] = useState({
        email: "",
        password: "",
        type: "user",
    });

    const onLogin = () => {
        if (loginData.email != "" && loginData.password != "") {
            setLoading(true)
            setErrorMsg("")
            props.postLoginUser(loginData, setLoading)
        } else {
            setErrorMsg("Wrong details")
        }
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setLoginData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    useEffect(() => {
        if (props.auth == null) {
            //window.location.href="/"
            console.log("empty")
        } else {
            // history.push("/dashboard")
            window.location.reload()
            //window.location.href="/"
        }
    }, [props.auth])


    useEffect(() => {
        if (props.auth?.success != null) {
            setErrorMsg("Success")
            console.log(props.auth?.success)
        } else if (props.errors != null) {
            if (props.errors?.data.error == "Unauthorized") {
                setErrorMsg("Wrong Email or Password !")
            } else {
                setErrorMsg(props.errors?.data.error)
            }
        }
    }, [props])

    return (
        <div className="loginBodyWrapper">

            <div className="row">

                <div className="col-sm-5 loginPagelogo">
                    <div className="authPageBanner"></div>
                </div>
                <div className="col-sm-6 ">
                    <div className="formBodyWrapper">
                        <div className="loginLogo">
                            <Image src={require("../../assets/logo.jpeg")} style={{ width: '100%', height: '100%' }} />
                        </div>
                        <div className="loginTitle">Log in to your account.</div>
                        <div className="errorMsg">{errorMsg}</div>
                        <div className="emailWrapper">
                            <InputField iconName={faEnvelope} name="email" onChange={onChange} placeholder="Email" type="email" />
                        </div>
                        <div className="emailWrapper">
                            <InputField iconName={faLock} name="password" onChange={onChange} placeholder="Password " type="password" className="passwordField" />
                        </div>
                        <div className="forgotPassword" onClick={() => window.location.href = "/forgotPassword"}>Forgot password ?</div>
                        <div className="formBtnwrapper">
                            <div className="loginBtn" onClick={() => loading ? null : onLogin()}>
                                {
                                    loading ?
                                        <Spinner animation="grow" size="sm" />
                                        :
                                        "Login"
                                }
                            </div>
                            <div className="hr"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    auth: state.auth.loginRes,
});
export default connect(mapStateToProps, { postLoginUser })(Login);

