import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../styles/Home.css";
import "../../styles/Responsive.css";
import InputField from "./components/InputField";
import { connect } from "react-redux";
import { postSignupUser } from "../../state-management/actions/auth/authActions";
import Spinner from 'react-bootstrap/Spinner'

const Signup = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [signupData, setSignupData] = useState({
        name: "",
        email: "",
        password: "",
        c_password: "",
        type: "user",
    });

    const onSignup = () => {
        if (signupData.email != "" && signupData.name != "" && signupData.password != "" && signupData.cpassword != "") {
            setLoading(true)
            setErrorMsg("")
            props.postSignupUser(signupData, setLoading)
        }else{
            setErrorMsg("Fill all details")
        }
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setSignupData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    useEffect(() => {
        if (props.auth?.success != null) {
            setErrorMsg("Success")
            window.location.href="/register"
            console.log(props.auth?.success)
        } else if (props.errors != null) {
            if (props.errors?.status == 401) {
                setErrorMsg("Wrong Email or Password !")
            } else {
                setErrorMsg("Something went wrong !")
            }
        }
    }, [props])

    return (
        <div className="loginBodyWrapper">

            <div className="formBodyWrapper signupBodyWrapper">
                <div className="loginTitle">CRM Register</div>
                <div className="errorMsg">{errorMsg}</div>
                <InputField name="name" onChange={onChange} placeholder="Name" type="text" />
                <InputField name="email" onChange={onChange} placeholder="Email" type="email" />
                <InputField name="password" onChange={onChange} placeholder="Password" type="password" className="passwordField" />
                <InputField name="c_password" onChange={onChange} placeholder="Confirm password " type="password" className="passwordConfirmField" />
                <div className="formBtnwrapper">
                    <div className="loginBtn" onClick={() => loading ? null : onSignup()}>
                        {
                            loading ?
                                <Spinner animation="grow" size="sm" />
                                :
                                "Register"
                        }
                    </div>
                    <div className="loginBtn" onClick={() => window.location.href = "/"}>Back to Login</div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    auth: state.auth.signupRes,
});
export default connect(mapStateToProps, { postSignupUser })(Signup);

