import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const InputField = (props) => {
    return (
        <div className="formInput">
            <div className="fieldIconWrapper">
                <FontAwesomeIcon icon={props.iconName} />
            </div>
            <input name={props.name} onChange={props.onChange} type={props.type} placeholder={props.placeholder} className={props.className} />
        </div>
    );
};

export default InputField;
