import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../styles/Home.css";
import "../../styles/Responsive.css";
import InputField from "./components/InputField";
import { connect } from "react-redux";
import { forgotPasswordLink } from "../../state-management/actions/auth/authActions";
import Spinner from 'react-bootstrap/Spinner'

const ForgotPassword = (props) => {

    const [loading, setLoading] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const [loginData, setLoginData] = useState({ email: "" });

    const onLogin = () => {
        if (loginData.email != "") {
            setLoading(true)
            setErrorMsg("")
            props.forgotPasswordLink(loginData, setLoading)
        } else {
            setErrorMsg("Enter email")
        }
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        setLoginData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    useEffect(() => {
        if (props.auth?.message == "Mail send successfully") {
            setErrorMsg("Success")
        } else if (props.errors != null) {
            if (props.errors?.status == 401) {
                setErrorMsg("Wrong Email or Password !")
            } else {
                setErrorMsg("Something went wrong !")
            }
        }

    }, [props])

    return (
        <div className="loginBodyWrapper">

            <div className="formBodyWrapper">
                <div className="loginTitle">Recover Password</div>
                <div className="errorMsg" style={{ color: errorMsg == "Success" ? 'green' : 'red' }}>
                    {errorMsg == "Success" ? props?.auth?.message : errorMsg}
                </div>
                <InputField name="email" onChange={onChange} placeholder="Enter your email" type="email" />
                <div className="formBtnwrapper">
                    <div className="loginBtn" onClick={() => loading ? null : onLogin()}>
                        {
                            loading ?
                                <Spinner animation="grow" size="sm" />
                                :
                                "Reset"
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    errors: state.errors.errors,
    auth: state.auth.get_recover_password_link,
});
export default connect(mapStateToProps, { forgotPasswordLink })(ForgotPassword);

