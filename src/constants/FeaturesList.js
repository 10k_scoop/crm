import { faBuilding, faCogs, faContactBook, faFolderTree, faHome, faHomeUser, faPhone, faShop, faUser, faUserFriends, faUsers } from "@fortawesome/free-solid-svg-icons";

export const AdminFeatures = [
    { name: "Dashboard", icon: faHome, redirect: "/companyDashboard" },
    { name: "Users", icon: faUsers, redirect: "/users" },
    { name: "Roles", icon: faUsers, redirect: "/showRoles" },
    { name: "Contacts", icon: faContactBook, redirect: "/showContacts" },
    { name: "Projects", icon: faHomeUser, redirect: "/showProjects" },
    { name: "Customers", icon: faShop, redirect: "/showCustomers" },
    { name: "Property Owners", icon: faHomeUser, redirect: "/showPropertyOwners" },
    { name: "Properties", icon: faBuilding, redirect: "/showProperties" },
    { name: "Tenant", icon: faUserFriends, redirect: "/showTenants" },
    { name: "Tenancy Contracts", icon: faUserFriends, redirect: "/showTenancyContracts" },
    { name: "Employees", icon: faUser, redirect: "/showEmployees" },
    { name: "Attendance", icon: faUser, redirect: "/showAttendance" },
    { name: "Settings", icon: faCogs, redirect: "/settings" },

]

export const SuperAdminFeatures = [
    { name: "Dashboard", icon: faHome, redirect: "/companyDashboard" },
    { name: "Branches", icon: faFolderTree, redirect: "/" },
    { name: "Users", icon: faUsers, redirect: "/users" },
    { name: "Roles", icon: faUsers, redirect: "/showRoles" },
    { name: "Contacts", icon: faContactBook, redirect: "/showContacts" },
    { name: "Projects", icon: faHomeUser, redirect: "/showProjects" },
    { name: "Customers", icon: faShop, redirect: "/showCustomers" },
    { name: "Property Owners", icon: faHomeUser, redirect: "/showPropertyOwners" },
    { name: "Properties", icon: faBuilding, redirect: "/showProperties" },
    { name: "Tenant", icon: faUserFriends, redirect: "/showTenants" },
    { name: "Tenancy Contracts", icon: faUserFriends, redirect: "/showTenancyContracts" },
    { name: "Employees", icon: faUser, redirect: "/showEmployees" },
    { name: "Attendance", icon: faUser, redirect: "/showAttendance" },
    { name: "Settings", icon: faCogs, redirect: "/settings" },
]

export const SuperOwnerFeatures = [
    { name: "Dashboard", icon: faHome, redirect: "/dashboard" },
    { name: "Companies", icon: faCogs, redirect: "/companies" },
]

export const SalesAgentFeatures = [
    { name: "Dashboard", icon: faHome, redirect: "/companyDashboard" },
    { name: "My Tasks", icon: faContactBook, redirect: "/showTask" },
    { name: "Contacts", icon: faContactBook, redirect: "/showContacts" },
    { name: "Customers", icon: faShop, redirect: "/showCustomers" },
    { name: "Call Logs", icon: faPhone, redirect: "/showCallLogs" },
    { name: "Property Owners", icon: faHomeUser, redirect: "/showPropertyOwners" },
    { name: "Properties", icon: faBuilding, redirect: "/showProperties" },
    { name: "Settings", icon: faCogs, redirect: "/settings" },
]