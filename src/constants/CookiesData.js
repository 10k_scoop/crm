import Cookies from "js-cookie";

export const role=()=>{
    let data = Cookies.get('userDetails')
    return JSON.parse(data)?.roles[0]?.name
}

export const userDetails=()=>{
    let data = Cookies.get('userDetails')
    return JSON.parse(data)
}

export const token=()=>{
    let token = Cookies.get('token')
    return token
}

export const companyDetails=()=>{
    let data = Cookies.get('companyDetails')
    return JSON.parse(data)
}