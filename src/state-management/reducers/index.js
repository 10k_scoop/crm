import { combineReducers } from 'redux';
import mainReducer from './mainReducer';
import errorReducer from './errorReducer';
import authReducer from '../reducers/auth/authReducer'
export default combineReducers({
    main: mainReducer,
    errors: errorReducer,
    auth: authReducer,
})