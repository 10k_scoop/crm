import {
  GET_LOGIN,
  GET_SIGNUP,
  GET_RECOVER_PASSWORD,
  GET_RECOVER_PASSWORD_LINK,
  SET_NEW_PASSWORD
} from "../../types/types";
const initialState = {
  currentUser: null,
  signupRes: null,
  loginRes: null,
  recoverPassword: null,
  get_recover_password_link: null,
  set_new_password: null
};
const authReducer = (state = initialState, action) => {
  switch (action.type) {

    case GET_SIGNUP:
      return {
        ...state,
        signupRes: action.payload,
      };
    case GET_LOGIN:
      return {
        ...state,
        loginRes: action.payload,
      };
    case GET_RECOVER_PASSWORD:
      return {
        ...state,
        recoverPassword: action.payload,
      };
    case GET_RECOVER_PASSWORD_LINK:
      return {
        ...state,
        get_recover_password_link: action.payload,
      };
    case SET_NEW_PASSWORD:
      return {
        ...state,
        set_new_password: action.payload,
      };
    default:
      return state;
  }
};

export default authReducer;
