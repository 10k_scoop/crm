import {
  GET_CURRENT_USER,
  SET_COMPANIES,
  GET_COMPANIES,
  EDIT_COMPANIES,
  DELETE_COMPANIES,
  GET_BRANCHES,
  CREATE_ROLES,
  GET_REVISION_TASK,
  EDIT_ROLES,
  GET_ROLES,
  GET_CONTACTS,
  CREATE_CONTACT,
  EDIT_CONTACT,
  GET_CUSTOMERS,
  CREATE_CUSTOMER,
  EDIT_CUSTOMER,
  GET_PROPERTY_OWNERS,
  CREATE_PROPERTY_OWNER,
  EDIT_PROPERTY_OWNER,
  GET_PROPERTIES,
  CREATE_PROPERTY,
  EDIT_PROPERTY,
  GET_TENANTS,
  CREATE_TENANT,
  EDIT_TENANT,
  GET_EMPLOYEES,
  CREATE_EMPLOYEE,
  EDIT_EMPLOYEE,
  GET_USERS,
  CREATE_USER,
  EDIT_USER,
  DASHBOARD,
  CREATE_TASK,
  EDIT_TASK,
  GET_TASKS,
  AUTO_TASK_ASSIGN,
  GET_CALL_LOGS,
  EDIT_CALL_LOG,
  DELETE_CALL_LOG,
  GET_TENANCY_CONTARCTS,
  DELETE_TENANCY_CONTARCT,
  EDIT_TENANCY_CONTARCT,
  CREATE_TENANCY_CONTARCT,
  GET_ATTENDANCE,
  EDIT_ATTENDANCE,
  CREATE_ATTENDANCE,
  GET_PROJECTS,
  EDIT_PROJECT,
  CREATE_PROJECT
} from "../types/types";

const initialState = {
  currentUser: null,
  get_companies: null,
  set_companies: null,
  edit_companies: null,
  delete_companies: null,
  get_branches: null,
  edit_roles: null,
  get_roles: null,
  create_roles: null,
  edit_contacts: null,
  get_contacts: null,
  create_contacts: null,
  edit_customer: null,
  get_customers: null,
  create_customer: null,
  edit_property_owner: null,
  get_property_owners: null,
  create_property_owner: null,
  edit_property: null,
  get_properties: null,
  create_property: null,
  edit_tenant: null,
  get_tenants: null,
  create_tenant: null,
  edit_employee: null,
  get_employees: null,
  create_employee: null,
  get_users: null,
  create_user: null,
  edit_user: null,
  get_tasks: null,
  create_task: null,
  edit_task: null,
  get_revision_task: null,
  auto_task_assign: null,
  get_call_logs: null,
  edit_call_log: null,
  delete_call_log: null,
  get_tenancy_contracts: null,
  edit_tenancy_contract: null,
  create_tenancy_contract: null,
  get_attendance: null,
  edit_attendance: null,
  create_attendance: null,
  get_projects:null,
  create_project:null,
  edit_project:null

};
const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CURRENT_USER:
      return {
        ...state,
        currentUser: action.payload,
      };
    case GET_COMPANIES:
      return {
        ...state,
        get_companies: action.payload,
      };
    case SET_COMPANIES:
      return {
        ...state,
        set_companies: action.payload,
      };
    case EDIT_COMPANIES:
      return {
        ...state,
        edit_companies: action.payload,
      };
    case DELETE_COMPANIES:
      return {
        ...state,
        delete_companies: action.payload,
      };
    case GET_BRANCHES:
      return {
        ...state,
        get_branches: action.payload,
      };
    case GET_ROLES:
      return {
        ...state,
        get_roles: action.payload,
      };
    case CREATE_ROLES:
      return {
        ...state,
        create_roles: action.payload,
      };
    case EDIT_ROLES:
      return {
        ...state,
        edit_roles: action.payload,
      };
    case GET_CONTACTS:
      return {
        ...state,
        get_contacts: action.payload,
      };
    case CREATE_CONTACT:
      return {
        ...state,
        create_contact: action.payload,
      };
    case EDIT_CONTACT:
      return {
        ...state,
        edit_contact: action.payload,
      };
    case GET_CUSTOMERS:
      return {
        ...state,
        get_customers: action.payload,
      };
    case CREATE_CUSTOMER:
      return {
        ...state,
        create_customer: action.payload,
      };
    case EDIT_CUSTOMER:
      return {
        ...state,
        edit_customer: action.payload,
      };
    case GET_PROPERTY_OWNERS:
      return {
        ...state,
        get_property_owners: action.payload,
      };
    case CREATE_PROPERTY_OWNER:
      return {
        ...state,
        create_property_owner: action.payload,
      };
    case EDIT_PROPERTY_OWNER:
      return {
        ...state,
        edit_property_owner: action.payload,
      };
    case GET_PROPERTIES:
      return {
        ...state,
        get_properties: action.payload,
      };
    case CREATE_PROPERTY:
      return {
        ...state,
        create_property: action.payload,
      };
    case EDIT_PROPERTY:
      return {
        ...state,
        edit_property: action.payload,
      };
    case GET_TENANTS:
      return {
        ...state,
        get_tenants: action.payload,
      };
    case CREATE_TENANT:
      return {
        ...state,
        create_tenant: action.payload,
      };
    case EDIT_TENANT:
      return {
        ...state,
        edit_tenant: action.payload,
      };

    case GET_EMPLOYEES:
      return {
        ...state,
        get_employees: action.payload,
      };
    case CREATE_EMPLOYEE:
      return {
        ...state,
        create_employee: action.payload,
      };
    case EDIT_EMPLOYEE:
      return {
        ...state,
        edit_employee: action.payload,
      };
    case GET_USERS:
      return {
        ...state,
        get_users: action.payload,
      };
    case CREATE_USER:
      return {
        ...state,
        create_user: action.payload,
      };
    case EDIT_USER:
      return {
        ...state,
        edit_user: action.payload,
      };
    case CREATE_TASK:
      return {
        ...state,
        create_task: action.payload,
      };
    case EDIT_TASK:
      return {
        ...state,
        edit_task: action.payload,
      };
    case GET_TASKS:
      return {
        ...state,
        get_tasks: action.payload,
      };
    case GET_REVISION_TASK:
      return {
        ...state,
        get_revision_task: action.payload,
      };
    case AUTO_TASK_ASSIGN:
      return {
        ...state,
        auto_task_assign: action.payload,
      };
    case GET_CALL_LOGS:
      return {
        ...state,
        get_call_logs: action.payload,
      };
    case EDIT_CALL_LOG:
      return {
        ...state,
        edit_call_log: action.payload,
      };
    case DELETE_CALL_LOG:
      return {
        ...state,
        delete_call_log: action.payload,
      };

    case GET_TENANCY_CONTARCTS:
      return {
        ...state,
        get_tenancy_contracts: action.payload,
      };

    case EDIT_TENANCY_CONTARCT:
      return {
        ...state,
        edit_tenancy_contract: action.payload,
      };

    case CREATE_TENANCY_CONTARCT:
      return {
        ...state,
        create_tenancy_contract: action.payload,
      };

    case GET_ATTENDANCE:
      return {
        ...state,
        get_attendance: action.payload,
      };

    case EDIT_ATTENDANCE:
      return {
        ...state,
        edit_attendance: action.payload,
      };

    case CREATE_ATTENDANCE:
      return {
        ...state,
        create_attendance: action.payload,
      };
    case GET_PROJECTS:
      return {
        ...state,
        get_projects: action.payload,
      };

    case EDIT_PROJECT:
      return {
        ...state,
        edit_project: action.payload,
      };

    case CREATE_PROJECT:
      return {
        ...state,
        create_project: action.payload,
      };

    default:
      return state;
  }
};

export default mainReducer;
