import axios from "axios";
import { baseUrl } from "../../helper";
import {
  EDIT_COMPANIES,
  GET_BRANCHES,
  GET_COMPANIES,
  GET_ERRORS,
  GET_LOGIN,
  GET_RECOVER_PASSWORD_LINK,
  GET_SIGNUP,
  SET_COMPANIES,
  SET_NEW_PASSWORD,
} from "../../types/types";


import Cookies from 'js-cookie'

export const postSignupUser = (data, setLoading) => (dispatch) => {
  try {
    axios
      .post(`${baseUrl}/admin/register?name=${data.name}&email=${data.email}&password=${data.password}&c_password=${data.c_password}`
      )
      .then((res) => {
        dispatch({ type: GET_SIGNUP, payload: res.data });
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: err.response });
      });
  } catch (e) {
    console.log(e)
  }
};
export const postLoginUser = (data, setLoading) => (dispatch) => {
  try {
    axios
      .post(`${baseUrl}/admin/login?email=${data.email}&password=${data.password}`)
      .then(async (res) => {
        setLoading(false);
        await Cookies.set('token', res.data?.token, { expires: 1 })
        await Cookies.set('userRole', JSON.stringify(res?.data?.userRoles), { expires: 1 })
        await Cookies.set('companyDetails', JSON.stringify(res?.data), { expires: 1 })
        await Cookies.set('userDetails', JSON.stringify(res?.data?.user), { expires: 1 })
        dispatch({ type: GET_LOGIN, payload: res.data });
        console.log(res?.data)
      })
      .catch((err) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: err.response });
      });
  } catch (e) {
    console.log(e)
  }
};

export const onLogout = (data, setLoading) => async (dispatch) => {
  try {
    await Cookies.remove('token', null)
    await Cookies.remove('userData', null)
    await Cookies.remove('access_token', null)
    await Cookies.remove('userDetails', null)
    console.log("Logged Out")
    window.location.reload()
    window.location.href = "/"
  } catch (e) {
    console.log(e.message)
  }
};

export const forgotPasswordLink = (data, setLoading) => (dispatch) => {
  try {
    axios
      .post(`${baseUrl}/password/forgot-password?email=${data.email}`)
      .then((res) => {
        dispatch({ type: GET_RECOVER_PASSWORD_LINK, payload: res.data });
        setLoading(false);
        // redirect user here
        //window.location.href = baseUrl;
      })
      .catch((err) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: err.response });
      });
  } catch (e) {
    console.log(e)
  }
};

// RESET Password
export const resetNewPassword = (data, setLoading) => (dispatch) => {

  try {
    fetch(`${baseUrl}/password/reset`, {
      method: "POST",
      headers: {
        Accept: 'application/form-data',
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        return response.json()
      })
      .then((res) => {
        setLoading(false);
        dispatch({ type: SET_NEW_PASSWORD, payload: res });
      })
      .catch((err) => {
        dispatch({ type: GET_ERRORS, payload: err.response });
        setLoading(false);
      });
  } catch (err) {
    dispatch({ type: GET_ERRORS, payload: err.response });
    setLoading(false);
    console.log(err.message)
  }
};

//Get Company List
export const getCompanies = (setLoading) => (dispatch) => {
  let auth_Token = Cookies.get('token')
  try {
    axios
      .get(`${baseUrl}/companies`, {
        headers: {
          Authorization: "Bearer " + auth_Token,
          "Content-Type": "application/json",
        },
      })
      .then((res) => {
        dispatch({ type: GET_COMPANIES, payload: res.data.data?.companies });
        setLoading(false);
        console.log(res.data)
      })
      .catch((err) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: err.response });
      });
  } catch (e) {
    console.log(e)
    dispatch({ type: GET_ERRORS, payload: e.response });
  }
};

//Create a Company
export const createCompany = (companyData, setLoading) => (dispatch) => {
  console.log(companyData)
  let auth_Token = Cookies.get('token')

  try {
    fetch(`${baseUrl}/companies`, {
      method: "POST",
      headers: {
        Authorization: "Bearer " + auth_Token,
        Accept: 'application/form-data',
        "Content-Type": "application/json",
      },
      body: JSON.stringify(companyData),
    })
      .then((res) => {
        dispatch({ type: SET_COMPANIES, payload: res.data });
        setLoading(false);
        window.location.href = "/"
      })
      .catch((err) => {
        setLoading(false);
        console.log(err)
        dispatch({ type: GET_ERRORS, payload: err.response });
      });
  } catch (e) {
    console.log(e.message)
    dispatch({ type: GET_ERRORS, payload: e.response });
  }
};


// Edit company
export const editCompany = (companyData, setLoading) => (dispatch) => {
  console.log(companyData)
  let auth_Token = Cookies.get('token')
  try {
    fetch(`${baseUrl}/companies/update/${companyData.id}`, {
      method: "POST",
      headers: {
        Authorization: "Bearer " + auth_Token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(companyData),
    })
      .then((res) => {
        dispatch({ type: EDIT_COMPANIES, payload: res.data });
        setLoading(false);
        window.location.href = "/"
      })
      .catch((err) => {
        setLoading(false);
        console.log(err)
        dispatch({ type: GET_ERRORS, payload: err.response });
      });
  } catch (e) {
    console.log(e.message)
    dispatch({ type: GET_ERRORS, payload: e.response });
  }
};


// Delete company
export const deleteCompany = (id, setLoading) => (dispatch) => {
  console.log(id)
  let auth_Token = Cookies.get('token')

  try {
    fetch(`${baseUrl}/companies/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + auth_Token,
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        dispatch({ type: GET_COMPANIES, payload: res.data });
        setLoading(false);
        window.location.href = "/"
      })
      .catch((err) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: err.response });
      });
  } catch (e) {
    console.log(e.message)
    dispatch({ type: GET_ERRORS, payload: e.response });
  }
};


// Search company
export const searchCompany = (data, setLoading) => (dispatch) => {

  let auth_Token = Cookies.get('token')
  data.created_by = parseInt(data.created_by)

  try {
    axios
      .post(`${baseUrl}/companies/filter`, data, {
        headers: {
          Authorization: "Bearer " + auth_Token,
          "Content-Type": "application/json",
        },
      })
      .then((res) => {
        dispatch({ type: GET_COMPANIES, payload: res.data.data });
        setLoading(false);
        //window.location.href = "/"
        console.log(res.data.data)
      })
      .catch((err) => {
        setLoading(false);
        console.log("Inside api")
        dispatch({ type: GET_ERRORS, payload: err.response });
      });
  } catch (e) {
    setLoading(false)
    console.log(e.message)
    dispatch({ type: GET_ERRORS, payload: e.response });
  }
};


//Show Branches of related Company
export const getMyBranches = (setLoading) => (dispatch) => {
  let auth_Token = Cookies.get('token')
  console.log(auth_Token)
  try {
    axios
      .get(`${baseUrl}/branches`, {
        headers: {
          Authorization: "Bearer " + auth_Token,
          "Content-Type": "application/json",
        },
      })
      .then((res) => {
        dispatch({ type: GET_BRANCHES, payload: res.data?.data });
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: err.response });
      });
  } catch (e) {
    console.log(e)
    dispatch({ type: GET_ERRORS, payload: e.response });
  }
};


//Create a Branch of comaopny
export const createBranch = (branchData, setLoading) => (dispatch) => {
  let auth_Token = Cookies.get('token')
  console.log(auth_Token)

  try {
    fetch(`${baseUrl}/branches`, {
      method: "POST",
      headers: {
        Authorization: "Bearer " + auth_Token,
        Accept: 'application/form-data',
        "Content-Type": "application/json",
      },
      body: JSON.stringify(branchData),
    })
      .then((res) => {
        dispatch({ type: SET_COMPANIES, payload: res.data });
        setLoading(false);
        window.location.href = "/"
      })
      .catch((err) => {
        setLoading(false);
        console.log(err)
        dispatch({ type: GET_ERRORS, payload: err.response });
      });
  } catch (e) {
    console.log(e.message)
    dispatch({ type: GET_ERRORS, payload: e.response });
  }
};


// Edit company Branch
export const editCompanyBranch = (branchData, setLoading) => (dispatch) => {
  let auth_Token = Cookies.get('token')
  console.log(branchData)
  try {
    fetch(`${baseUrl}/branches/update/${branchData.branch_id}`, {
      method: "POST",
      headers: {
        Authorization: "Bearer " + auth_Token,
        Accept: 'application/form-data',
        "Content-Type": "application/json",
      },
      body: JSON.stringify(branchData),
    })
      .then((res) => {
        setLoading(false);
        window.location.href = "/"
      })
      .catch((err) => {
        setLoading(false);
        console.log(err)
        dispatch({ type: GET_ERRORS, payload: err.response });
      });
  } catch (e) {
    console.log(e.message)
    dispatch({ type: GET_ERRORS, payload: e.response });
  }
};


// Delete company Branch
export const deleteCompanyBranch = (id, setLoading) => (dispatch) => {
  console.log(id)
  let auth_Token = Cookies.get('token')

  try {
    fetch(`${baseUrl}/branches/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + auth_Token,
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        dispatch({ type: GET_BRANCHES, payload: res.data });
        setLoading(false);
        window.location.href = "/"
      })
      .catch((err) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: err.response });
      });
  } catch (e) {
    console.log(e.message)
    dispatch({ type: GET_ERRORS, payload: e.response });
  }
};

