import axios from "axios";
import { baseUrl } from "../helper.js"
import Cookies from 'js-cookie'
import { AUTO_TASK_ASSIGN, CREATE_ATTENDANCE, CREATE_CONTACT, CREATE_CUSTOMER, CREATE_EMPLOYEE, CREATE_PROJECT, CREATE_PROPERTY, CREATE_PROPERTY_OWNER, CREATE_ROLES, CREATE_TASK, CREATE_TENANT, CREATE_USER, EDIT_ATTENDANCE, EDIT_CALL_LOG, EDIT_CONTACT, EDIT_CUSTOMER, EDIT_EMPLOYEE, EDIT_PROJECT, EDIT_PROPERTY, EDIT_PROPERTY_OWNER, EDIT_ROLES, EDIT_TENANCY_CONTARCT, EDIT_TENANT, EDIT_USER, GET_ATTENDANCE, GET_CALL_LOGS, GET_CONTACTS, GET_CUSTOMERS, GET_EMPLOYEES, GET_ERRORS, GET_PROJECTS, GET_PROPERTIES, GET_PROPERTY_OWNERS, GET_REVISION_TASK, GET_ROLES, GET_TASKS, GET_TENANCY_CONTARCTS, GET_TENANTS, GET_USERS } from "../types/types";


//Show Roles 
export const showRoles = (setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(auth_Token)
    try {
        axios
            .get(`${baseUrl}/roles`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_ROLES, payload: res.data });
                setLoading(false);
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


//Create role
export const createRole = (name, permissions, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    let form = {
        name: name,
        permissions: permissions
    }

    try {
        fetch(`${baseUrl}/roles`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                'Access-Control-Allow-Origin': '*',
                "Content-Type": "application/json",
            },
            body: JSON.stringify(form),
        })
            .then((res) => {
                dispatch({ type: CREATE_ROLES, payload: res.data });
                setLoading(false);
                window.location.href = "/showRoles"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


// Edit role
export const editRole = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    //console.log(data)
    try {
        fetch(`${baseUrl}/roles/update/${data.id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_ROLES, payload: res.data });
                setLoading(false);
                window.location.href = "/showRoles"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


// Delete Role
export const deleteRole = (id, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/roles/${id}`, {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + auth_Token,
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then((res) => {
                dispatch({ type: GET_ROLES, payload: res.data });
                setLoading(false);
                window.location.href = "/showRoles"
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

//Show Contacts 
export const showContacts = (setLoading,) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/contacts`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_CONTACTS, payload: res.data });
                setLoading(false);
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

//Show Unassigned Contacts 
export const showUnassignedContacts = (setLoading,) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/unassignedcontacts`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_CONTACTS, payload: res.data });
                setLoading(false);
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


//Create contacts
export const createContact = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/contacts`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: CREATE_CONTACT, payload: res.data });
                setLoading(false);
                window.location.href = "/showContacts"
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


// Edit contacts
export const editContact = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(data)
    try {
        fetch(`${baseUrl}/contacts/update/${data.id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_CONTACT, payload: res.data });
                setLoading(false);
                window.location.href = "/showContacts"
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


// Delete contacts
export const deleteContact = (id, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(id)
    try {
        fetch(`${baseUrl}/contacts/${id}`, {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + auth_Token,
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then((res) => {
                dispatch({ type: GET_CONTACTS, payload: res.data });
                setLoading(false);
                window.location.href = "/showContacts"
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


//Show Customers 
export const showCustomers = (setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/customers`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_CUSTOMERS, payload: res.data.data });
                setLoading(false);
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};



//Create customers
export const createCustomer = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/customers`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: CREATE_CUSTOMER, payload: res.data });
                setLoading(false);
                window.location.href = "/showCustomers"
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


// Edit Customers
export const editCustomer = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(data)
    try {
        fetch(`${baseUrl}/customers/${data.id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_CUSTOMER, payload: res.data });
                setLoading(false);
                ///window.location.href = "/showCustomers"
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};





// Delete Customers
export const deleteCustomer = (id, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/customers/${id}`, {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + auth_Token,
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then((res) => {
                dispatch({ type: GET_CUSTOMERS, payload: res.data });
                setLoading(false);
                //window.location.href = "/showCustomers"
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};




//Show Property Owners 
export const showPropertyOwners = (setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/propertyowners`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_PROPERTY_OWNERS, payload: res.data.data });
                setLoading(false);
                ///console.log(res.data.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};



//Create PropertyOwner
export const createPropertyOwner = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/propertyowners`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: CREATE_PROPERTY_OWNER, payload: res.data });
                setLoading(false);
                window.location.href = "/showPropertyOwners"
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


// Edit PropertyOwner 
export const editPropertyOwner = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(data)
    try {
        fetch(`${baseUrl}/propertyowners/update/${data.id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_PROPERTY_OWNER, payload: res.data });
                setLoading(false);
                ///window.location.href = "/showCustomers"
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};





// Delete PropertyOwner 
export const deletePropertyOwner = (id, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/propertyowners/${id}`, {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + auth_Token,
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then((res) => {
                dispatch({ type: GET_PROPERTY_OWNERS, payload: res.data });
                setLoading(false);
                window.location.href = "/showPropertyOwners"
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


//Show Properties
export const showProperties = (setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/properties`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_PROPERTIES, payload: res.data.data });
                setLoading(false);
                ///console.log(res.data.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

//Create Property
export const createProperty = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/properties`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: CREATE_PROPERTY, payload: res.data });
                setLoading(false);
                window.location.href = "/showProperties"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Edit Property 
export const editProperty = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(data)
    try {
        fetch(`${baseUrl}/properties/update/${data.id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_PROPERTY, payload: res.data });
                setLoading(false);
                //window.location.href = "/showProperties"
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Delete Property 
export const deleteProperty = (id, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/properties/${id}`, {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + auth_Token,
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then((res) => {
                dispatch({ type: GET_PROPERTIES, payload: res.data });
                setLoading(false);
                //window.location.href = "/showProperties"
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};



//Show Tenants
export const showTenants = (setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/tenants`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_TENANTS, payload: res.data.data });
                setLoading(false);
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

//Create Tenant
export const createTenant = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/tenants`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: CREATE_TENANT, payload: res.data });
                setLoading(false);
                window.location.href = "/showTenants"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Edit Tenant 
export const editTenant = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(data)
    try {
        fetch(`${baseUrl}/tenants/update/${data.id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_TENANT, payload: res.data });
                setLoading(false);
                //window.location.href = "/showTenants"
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Delete Tenant 
export const deleteTenant = (id, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/tenants/${id}`, {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + auth_Token,
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then((res) => {
                dispatch({ type: GET_TENANTS, payload: res.data });
                setLoading(false);
                //window.location.href = "/showTenants"
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};



//Show Employees
export const showEmployees = (setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/employees`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_EMPLOYEES, payload: res.data.data });
                setLoading(false);
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

//Create Employee
export const createEmployee = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/employees`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: CREATE_EMPLOYEE, payload: res.data });
                setLoading(false);
                //window.location.href = "/showEmployees"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Edit Employee 
export const editEmployee = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(data)
    try {
        fetch(`${baseUrl}/employees/update/${data.id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_EMPLOYEE, payload: res.data });
                setLoading(false);
                //window.location.href = "/showEmployees"
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Delete Employee 
export const deleteEmployee = (id, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/employees/${id}`, {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + auth_Token,
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then((res) => {
                dispatch({ type: GET_EMPLOYEES, payload: res.data });
                setLoading(false);
                //window.location.href = "/showEmployees"
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

//Show Users
export const showUsers = (setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/branchusers`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                },
            })
            .then((res) => {
                dispatch({ type: GET_USERS, payload: res.data.data });
                setLoading(false);
                //console.log(res.data.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

//Create User
export const createUser = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(data)
    try {
        fetch(`${baseUrl}/branchusers`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: CREATE_USER, payload: res.data });
                setLoading(false);
                //window.location.href = "/users"
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Edit User 
export const editUser = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(data)
    try {
        fetch(`${baseUrl}/branchusers/update/${data.id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_USER, payload: res.data });
                setLoading(false);
                window.location.href = "/users"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Delete User 
export const deleteUser = (id, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/branchusers/${id}`, {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + auth_Token,
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then((res) => {
                dispatch({ type: GET_USERS, payload: res.data });
                setLoading(false);
                window.location.href = "/users"
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};



//Create Task || Assign Lead
export const createTask = (data, setLoading, setShowModal) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        fetch(`${baseUrl}/tasks`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((response) => response.json())
            .then((res) => {
                dispatch({ type: CREATE_TASK, payload: res });
                setLoading(false);
                setShowModal(false)
                window.location.href = "/showContacts"
                console.log(res)
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

//Show Tasks
export const showTasks = (setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/tasks`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_TASKS, payload: res.data });
                setLoading(false);
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

//Revision Task 
export const showRevisionTask = (id, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        axios
            .get(`${baseUrl}/revisionTask/${id}`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_REVISION_TASK, payload: res.data });
                setLoading(false);
                //console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Update Task
export const updateTask = (id, data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(data)
    try {
        fetch(`${baseUrl}/tasks/update/${id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_USER, payload: res.data });
                setLoading(false);
                console.log(res)
                //window.location.href = "/"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Auto Assaign Task
export const autoAssignTask = (data, setLoading, setAutoAssign) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/automaticStatus`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: AUTO_TASK_ASSIGN, payload: res.data });
                setLoading(false);
                setAutoAssign(data.status)
                //window.location.href = "/showContacts"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

//Show Call Logs
export const showCallLogs = (setLoading,) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/calls`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_CALL_LOGS, payload: res.data });
                setLoading(false);
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Eidt CallLogs
export const editCallLog = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(data)
    try {
        fetch(`${baseUrl}/calls/update/${data.id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_CALL_LOG, payload: res.data });
                setLoading(false);
                //window.location.href = "/showCallLogs"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Delete Call Logs 
export const deleteCallLog = (id, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(id)
    try {
        fetch(`${baseUrl}/calls/${id}`, {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + auth_Token,
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then((res) => {
                dispatch({ type: GET_CALL_LOGS, payload: res.data });
                setLoading(false);
                console.log(res)
                //window.location.href = "/showCallLogs"
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


//Show Tenancy Contracts
export const showTenancyContracts = (setLoading,) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/tenancycontracts`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_TENANCY_CONTARCTS, payload: res.data });
                setLoading(false);
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Eidt Tenancy Contract
export const editTenancyContract = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        fetch(`${baseUrl}/tenancycontracts/update/${data.id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_TENANCY_CONTARCT, payload: res.data });
                setLoading(false);
                //window.location.href = "/showCallLogs"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Delete Tenancy Contracts
export const deleteTenancyContract = (id, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(id)
    try {
        fetch(`${baseUrl}/tenancycontracts/${id}`, {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + auth_Token,
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then((res) => {
                dispatch({ type: GET_TENANCY_CONTARCTS, payload: res.data });
                setLoading(false);
                console.log(res)
                //window.location.href = "/showCallLogs"
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


//Create Attendance
export const createAttendance = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        fetch(`${baseUrl}/attendance`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((response) => response.json())
            .then((res) => {
                dispatch({ type: CREATE_ATTENDANCE, payload: res });
                setLoading(false);
                window.location.href = "/showAttendance"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

//Edit Attendance
export const editAttendance = (data, setLoading, setShowModal) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        fetch(`${baseUrl}/attendance/update/${data.id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_ATTENDANCE, payload: res.data });
                setLoading(false);
                //window.location.href = "/showAttendance"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};


//Show Attendance
export const showAttendance = (setLoading,) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/attendance`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_ATTENDANCE, payload: res.data });
                setLoading(false);
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};



//Show Projects
export const showProjects = (setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    try {
        axios
            .get(`${baseUrl}/projects`, {
                headers: {
                    Authorization: "Bearer " + auth_Token,
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((res) => {
                dispatch({ type: GET_PROJECTS, payload: res.data.projects });
                setLoading(false);
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

//Create Project
export const createProject = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/projects`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: CREATE_PROJECT, payload: res.data });
                setLoading(false);
                window.location.href = "/showProjects"
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Edit Project 
export const editProject = (data, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')
    console.log(data)
    try {
        fetch(`${baseUrl}/projects/update/${data.id}`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth_Token,
                Accept: 'application/form-data',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(data),
        })
            .then((res) => {
                dispatch({ type: EDIT_PROJECT, payload: res.data });
                setLoading(false);
                //window.location.href = "/showProjects"
                console.log(res.data)
            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};

// Delete Project 
export const deleteProject = (id, setLoading) => (dispatch) => {
    let auth_Token = Cookies.get('token')

    try {
        fetch(`${baseUrl}/projects/${id}`, {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + auth_Token,
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then((res) => {
                dispatch({ type: GET_PROJECTS, payload: res.data });
                setLoading(false);
                //window.location.href = "/showProjects"
            })
            .catch((err) => {
                setLoading(false);
                dispatch({ type: GET_ERRORS, payload: err.response });
            });
    } catch (e) {
        console.log(e.message)
        dispatch({ type: GET_ERRORS, payload: e.response });
    }
};
