// Error types
export const GET_ERRORS = "GET_ERRORS";

//Signup Type
export const GET_SIGNUP = "GET_SIGNUP";
export const PUT_VERIFY_EMAIL = "PUT_VERIFY_EMAIL";

//GET_CURRENT_USER  Type
export const GET_CURRENT_USER = "GET_CURRENT_USER";

//Login Type
export const GET_LOGIN = "GET_LOGIN";

//Recover Type
export const GET_RECOVER_PASSWORD = "GET_RECOVER_PASSWORD";
export const SET_NEW_PASSWORD = "SET_NEW_PASSWORD";
export const GET_RECOVER_PASSWORD_LINK = "GET_RECOVER_PASSWORD_LINK";


//Companies type
export const GET_COMPANIES = "GET_COMPANIES";
export const SET_COMPANIES = "SET_COMPANIES";
export const EDIT_COMPANIES = "EDIT_COMPANIES";
export const DELETE_COMPANIES = "DELETE_COMPANIES";

//branches type
export const GET_BRANCHES = "GET_BRANCHES";

//Roles type
export const GET_ROLES = "GET_ROLES";
export const EDIT_ROLES = "EDIT_ROLES";
export const CREATE_ROLES = "CREATE_ROLES";


//Contact types
export const GET_CONTACTS = "GET_CONTACTS";
export const EDIT_CONTACT = "EDIT_CONTACT";
export const CREATE_CONTACT = "CREATE_CONTACT";

//Contact types
export const GET_CUSTOMERS = "GET_CUSTOMERS";
export const EDIT_CUSTOMER = "EDIT_CUSTOMER";
export const CREATE_CUSTOMER = "CREATE_CUSTOMER";


//Property Owners types
export const GET_PROPERTY_OWNERS = "GET_PROPERTY_OWNERS";
export const EDIT_PROPERTY_OWNER = "EDIT_PROPERTY_OWNER";
export const CREATE_PROPERTY_OWNER = "CREATE_PROPERTY_OWNER";

//Property Owners types
export const GET_PROPERTIES = "GET_PROPERTIES";
export const EDIT_PROPERTY = "EDIT_PROPERTY";
export const CREATE_PROPERTY = "CREATE_PROPERTY";


//Tenant types
export const GET_TENANTS = "GET_TENANTS";
export const EDIT_TENANT = "EDIT_TENANT";
export const CREATE_TENANT = "CREATE_TENANT";

//EMPLOYEES types
export const GET_EMPLOYEES = "GET_EMPLOYEES";
export const EDIT_EMPLOYEE = "EDIT_EMPLOYEE";
export const CREATE_EMPLOYEE = "CREATE_EMPLOYEE";

//User types
export const GET_USERS = "GET_USERS";
export const EDIT_USER = "EDIT_USER";
export const CREATE_USER = "CREATE_USER";


export const DASHBOARD = "DASHBOARD";

//Task assign types
export const GET_TASKS = "GET_TASKS";
export const EDIT_TASK = "EDIT_TASK";
export const CREATE_TASK = "CREATE_TASK";
export const GET_REVISION_TASK = "GET_REVISION_TASK";
export const AUTO_TASK_ASSIGN = "AUTO_TASK_ASSIGN";

//Call logs types
export const GET_CALL_LOGS = "GET_CALL_LOGS";
export const EDIT_CALL_LOG = "EDIT_CALL_LOG";
export const DELETE_CALL_LOG = "DELETE_CALL_LOG";

//Tenancy Contracts logs types
export const GET_TENANCY_CONTARCTS = "GET_TENANCY_CONTARCTS";
export const EDIT_TENANCY_CONTARCT = "EDIT_TENANCY_CONTARCT";
export const CREATE_TENANCY_CONTARCT = "CREATE_TENANCY_CONTARCT";


//Attendance types
export const GET_ATTENDANCE = "GET_ATTENDANCE";
export const EDIT_ATTENDANCE = "EDIT_ATTENDANCE";
export const CREATE_ATTENDANCE = "CREATE_ATTENDANCE";

//Project types
export const GET_PROJECTS = "GET_PROJECTS";
export const EDIT_PROJECT = "EDIT_PROJECT";
export const CREATE_PROJECT = "CREATE_PROJECT";